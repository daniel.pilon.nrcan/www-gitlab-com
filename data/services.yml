- id: training_sessions_basics
  name:     GitLab with Git Basics Training
  sow_name: Basic Training
  desc_url: /services/education/gitlab-basics/
  short_desc: GitLab training on using GitLab for source control management, as well as an overview of basic git concepts and command-line instructions.
  long_desc: |
    This class is designed to provide users with an introduction to GitLab. It starts with an overview of what GitLab is and of the GitLab web GUI. It then focuses on Git command line. The class includes live demonstrations of the use of GitLab as well as interactive sessions that allow users to experience it for themselves. 
    <br/><br/>
    We’ll start first with an overview of GitLab so you can learn the basics about what GitLab does and why DevOps teams use it. Then we’ll dive into Git, the version control system used by GitLab for source code management (SCM). Attendees will learn and practice fundamental Git concepts and commands. Throughout the course flow we'll provide demos and hands-on practice with the fundamental processes and tasks teams work on in GitLab, such as committing changes, creating branches and merge requests, using a CI/CD pipeline, and accessing security scanning.  
  target_audience: Any team member new to Git and GitLab
  learning_objectives:
    - Explain what GitLab is and why teams use it
    - Perform basic Git commands for branching, merging, and remote work
    - Apply fundamental concepts and skills using GitLab within the DevOps lifecycle
  prereqs:
    - Agile development
    - Source code management
  preresources:
    - name: Why you should move to Git
      href: https://www.youtube.com/watch?v=j16QHjv29A0
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  public_price: 399 (not yet available - coming soon)
  facility_price: 2000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-BAS
  class_size: Up to 20 students
  standard_sow: https://docs.google.com/document/d/1tZxq2Pkig_6ZDfxF_ha7iVHIuTr4NtIZvMS2o0Sh88Y/edit
  
- id: training_sessions_basics_remote
  name:     GitLab with Git Basics Training - Remote Delivery
  sow_name: Basic Training - Remote SOW
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 3000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-BAS-R
  class_size: Up to 12 students recommended
  standard_sow: https://docs.google.com/document/d/1wJ3a1NfI0SyA1KLweqT6U75oByA5oQT16XyDkMYZGr8/edit?usp=sharing

- id: training_sessions_pm
  name:     GitLab for Project Managers Training
  sow_name:     GitLab for Project Managers Training
  desc_url: /services/education/pm/
  short_desc: GitLab training on using GitLab issues, epics, and other project management tools to manage projects, products, and business processes.
  long_desc: |
    This class is designed to provide users with an introduction to GitLab's Plan stage - where users can manage software products or other projects using issues.  It focuses on the various tools available, including issues, epics, milestones, burndown charts, kanban boards, and agile portfolio management tools.
  target_audience: Project managers
  learning_objectives:
    - Set up projects by creating issues, labels, milestones, and groups
    - Manage projects using GitLab boards, epics, and roadmaps
    - Use GitLab to develop portfolio plans
  prereqs:
    - Agile development
    - Source code management
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-PM
  class_size: Up to 20 students
  standard_sow: https://docs.google.com/document/d/1m7wA05YjvgH1FmwJBCE3v4xDe-FA37xU2Saj38XANy8/edit

- id: training_sessions_pm_remote
  name:     GitLab for Project Managers Training - Remote Delivery
  sow_name:     Project Management Training - Remote SOW
  desc_url: /services/education/pm/
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 3000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-PM-R
  class_size: Up to 12 students recommended
  standard_sow: https://docs.google.com/document/d/1hXSgaOxMrnQrWlI3LaMY-pnDERbfTRcnEx4l3kf3lc0/edit?usp=sharing

- id: training_sessions_cicd
  name:     GitLab CI/CD Training
  sow_name: GitLab CI CD Training
  desc_url: /services/education/gitlab-ci
  short_desc: GitLab training on using the Verify, Package, and Release stages to support your team's CI/CD efforts.
  long_desc: |
    This class covers setting up continuous integration/continuous deployment (CI/CD) for your projects. It starts with a round-robin discussion of where your team is at with CI/CD today. It then focuses on what is CI/CD, why it should be used, and how to implement within GitLab. This class includes live demonstrations of the use of GitLab.
  level:    1 - Product
  target_audience: Technical Project Leads
  learning_objectives:
    - Describe what CI/CD is
    - Explain how runners work
    - Set up and configure CI/CD and runners
    - Verify a new feature
    - Scope and persist variables at various levels
    - Scaffold out the basics of a test, build, review, and deploy pipeline leveraging feature/topic branching as the review mechanism
    - Release and deployment workflow
    - Artifacts and dependency caching
    - Building and deploying images to GitLab registry    
  prereqs:
    - Agile development
    - Source code management
    - Continuous Integration/Continuous Delivery and Deployment advantages    
  preresources:
    - name: Why you should move to Git
      href: https://www.youtube.com/watch?v=j16QHjv29A0
    - name: GitLab CI/CD overview
      href: https://about.gitlab.com/features/gitlab-ci-cd/
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  facility_price: 2000
  unit: session
  duration: 1
  sku: GL-CICD
  standard_sow: https://docs.google.com/document/d/1ncYAJu2Snq2DYvA1Ty0VZNhPKqXWJi-8_tOtA16uSZU/edit

- id: training_sessions_cicd_remote
  name:     GitLab CI/CD Training - Remote Delivery
  sow_name: CI CD Training - Remote SOW
  desc_url: /services/education/gitlab-ci
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 3000
  unit: session
  duration: 1
  sku: GL-CICD-R
  standard_sow: https://docs.google.com/document/d/1p9fO5y-nob05vAoIeYuqLZjJ_ncsrb59b3eCdF9jjUI/edit?usp=sharing

- id: training_sessions_admin
  name:     GitLab for System Administrators Training
  sow_name: GitLab for System Administrators Training
  desc_url: /services/education/admin
  short_desc: GitLab training about the administrative functions required to configure and maintain your self-managed GitLab instance.
  long_desc: |
    The course covers all required installation, configuration, maintenance and backup administrative tasks for setting up and managing a GitLab instance. 
  level:    1 - Product
  target_audience: System Administrators
  learning_objectives:
    - Install GitLab
    - Configure basic settings
    - Add and remove users and adjust settings
    - Configure optional settings and integrations
    - Performing backups, restores, and upgrades
    - Monitor system performance
    - Perform recommended debugging procedures
  prereqs:
    - What Git is and how to use it
  preresources:
    - name: Why you should move to Git
      href: https://www.youtube.com/watch?v=j16QHjv29A0
    - name: Git-ing Started with Git
      href: https://www.youtube.com/watch?v=Ce5nz5n41z4&list=PLFGfElNsQthbTRBiMVV2JTQfUUpIlqBEw
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  facility_price: 2000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-ADM
  standard_sow: https://docs.google.com/document/d/1Y9fnaIT3_T_2hnN5S5K_4HND0psPdmmzE5RImdEdS_Q/edit

- id: training_sessions_admin_remote
  name:     GitLab for System Administrators Training - Remote Delivery
  sow_name: System Administrator Training - Remote SOW
  desc_url: /services/education/admin
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 3000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-ADM-R
  standard_sow: https://docs.google.com/document/d/13f9OB6nhAkSmB5hvMPfA_bc2MJIaFFlGjY3L7VzaWhc/edit?usp=sharing

- id: training_sessions_specialized
  name:     GitLab Specialized Training
  sow_name:     Specialized Training
  desc_url: /services/specialized/
  short_desc: GitLab training customized to your specific needs.
  long_desc: |
      This course is great for organizations looking to customize the training they receive specific to their needs.
  level:    1 - Product
  category: Education
  maturity: Viable
  planned-maturity: Lovable
  price: 5000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: N/A
  standard_sow: #TODO

- id: training_sessions_devops
  name:     GitLab DevOps Fundamentals Training
  sow_name:     DevOps Fundamentals Training
  desc_url: /services/education/devops-fundamentals
  long_desc: |
    Concurrent DevOps is a new way of thinking about how we create and ship software. Rather than organizing work in a sequence of steps and handoffs, the power of working concurrently is in unleashing collaboration across the organization. In this course we’ll walk through the fundamentals of using GitLab as a complete DevOps platform. Attendees will gain hands-on experience using GitLab throughout the lifecycle stages of Manage, Plan, Create, Verify, Package, Secure, Release, Configure, Monitor, and Defend.
  level:    1 - Product
  category: Education
  target_audience: |
    All DevOps team members including:
      <ul>
        <li>Technical Leads</li>
        <li>Project managers</li>
        <li>Developers</li>
        <li>Engineers</li>
        <li>Security specialists</li>
        <li>System Administrators</li>
      </ul>
  learning_objectives: 
    - Explain what Git and GitLab are and how they work
    - Describe how GitLab facilitates a concurrent devops lifecycle
    - Use Git and GitLab throughout each stage of the concurrent DevOps lifecycle
  prereqs:
    - Agile development
    - YAML
    - Markdown
    - Code creation tools used at your organization
  preresources:
    - name: GitLab Demo
      href: https://www.youtube.com/watch?v=XbE8IXlfTJg
    - name: GitLab Complete DevOps
      href: https://www.youtube.com/watch?v=68rGlAihKFw  
  maturity: Viable
  planned-maturity: Lovable
  price: 20000
  unit: session
  facility_price: 8000
  class_size: Up to 20 students
  duration: 4
  duration_desc: 4 days
  sku: GL-DOF
  standard_sow: https://docs.google.com/document/d/15tI6A8nU9pYIn8nPyeO7FTW-PJXSDfYCrlCgMJwzMYI/edit?usp=sharing

- id: integration_jira
  name:     JIRA Integration
  desc_url: /services/integration
  level:    1 - Product
  category: Integration
  maturity: Viable
  planned-maturity: Lovable
  price: 2400
  duration: 1
  duration_desc: ~1-3 days
  sku: GL-JIRA
  standard_sow: https://docs.google.com/document/d/1QwlpyetVQ_J27Vp8quZsayu7xgJxk5wdcnmNK7OvAiQ/edit

- id: integration_jenkins
  name:     Jenkins Integration
  desc_url: /services/integration
  level:    1 - Product
  category: Integration
  maturity: Viable
  planned-maturity: Lovable
  price: 2400
  duration: 1
  duration_desc: ~1-3 days
  sku: GL-JENKINS
  standard_sow: https://docs.google.com/document/d/1sQWzVVpF4U9mKYgZ2VEPcMDznjQ0qlO8O5ZuTwAMCZ0/edit

- id: integration_ldap
  name:     LDAP Integration
  desc_url: /services/integration
  level:    1 - Product
  category: Integration
  maturity: Viable
  planned-maturity: Lovable
  price: 2400
  duration: 1
  duration_desc: ~1-3 days
  sku: GL-LDAP
  standard_sow: https://docs.google.com/document/d/1pIgzOv4p1MAxfIn6n3jYJ_0LsupDnNPFDCg0WebG0VQ/edit#

- id: migration_jenkins
  name: Jenkins Migration
  desc_url: /services/migration/jenkins
  level: 1 - Product
  category: Migration
  maturity: Minimal
  planned-maturity: Lovable
  price: TBD
  duration: TBD

- id: training_sessions_is
  name:     GitLab InnerSourcing Training
  sow_name:     GitLab InnerSourcing Training
  desc_url: /services/education/innersourcing-course/
  short_desc: GitLab training  on benefits and best practices for adopting an InnerSourcing approach across development teams.
  long_desc: |
    This hands-on workshop covers what InnerSourcing is, the key components needed for InnerSourcing to be successful, and how it will benefit you and your company. Attendees will learn how GitLab can help drive collaboration and consistency throughout the organization, and begin to apply best practices used by GitLab's own teams.
  target_audience: Development team members
  learning_objectives:
    - Define What InnerSourcing is
    - Identify the key components needed for InnerSourcing to be successful
    - Assess how it will benefit you and your company
    - Explain GitLabs’s CI/CD Functions
    - Apply GitLab's Permission Model
    - Describe GitLabs’s Package and Release Stages
    - Apply InnerSourcing Best Practices
    - Apply InnerSourcing functions within GitLab
  prereqs:
    - Agile development
    - Source code management
  level:    1 - Product
  category: Education
  maturity: Minimal
  planned-maturity: Lovable
  price: 5000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-IS
  class_size: Up to 20 students
  standard_sow: https://docs.google.com/document/d/1snRZB0VYZE8dvr5Vl2ihOySWeDCJDr8kxdGtTghiGVg/edit?usp=sharing

- id: training_sessions_is_remote
  name:     GitLab InnerSourcing Training - Remote Delivery
  sow_name:     GitLab InnerSourcing Training - Remote SOW
  desc_url: /services/education/innersourcing-course/
  short_desc: GitLab training  on benefits and best practices for adopting an InnerSourcing approach across development teams.
  long_desc: |
    This hands-on workshop covers what InnerSourcing is, the key components needed for InnerSourcing to be successful, and how it will benefit you and your company. Attendees will learn how GitLab can help drive collaboration and consistency throughout the organization, and begin to apply best practices used by GitLab's own teams.
  target_audience: Development team members
  learning_objectives:
    - Define What InnerSourcing is
    - Identify the key components needed for InnerSourcing to be successful
    - Assess how it will benefit you and your company
    - Explain GitLabs’s CI/CD Functions
    - Apply GitLab's Permission Model
    - Describe GitLabs’s Package and Release Stages
    - Apply InnerSourcing Best Practices
    - Apply InnerSourcing functions within GitLab
  prereqs:
    - Agile development
    - Source code management
  level:    1 - Product
  category: Education
  maturity: Minimal
  planned-maturity: Lovable
  price: 3000
  unit: session
  duration: 1
  duration_desc: 1 day
  sku: GL-IS-R
  class_size: Up to 20 students
  standard_sow: https://docs.google.com/document/d/1-EcMLybE-gGBgNBbmxc2hrMdF8UbhGKYlbDcrfTM_dE/edit?usp=sharing
