---
layout: markdown_page
title: "Real-Time Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | March 12, 2020 |
| Target End Date | May 31, 2020 |
| Slack           | [#wg_real-time](https://app.slack.com/client/T02592416/CUX9Z2N66) (only accessible from within the company) |
| Google Doc      | [Real-Time Working Group Agenda](https://docs.google.com/document/d/1eqwiGKifpnE4XTog0dB4Lgb-Bx0cc8g8OejmWDoZabs/edit#) (only accessible from within the company) |
| Feature Issue   | [View Real-time updates of assignees in Issues/Merge Requests sidebar](https://gitlab.com/gitlab-org/gitlab/issues/17589)             |
| Associated OKRs | [Plan: Support incremental ACV](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6594) |

## Business Goal

To ship one real-time feature to self-hosted customers.

### Exit Criteria

 (✅ Done, ✏️ In-progress)               

### Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Christopher Lefelhocz | Senior Director of Engineering |
| Facilitator           | John Hope             | Engineering Manager, Plan      |
| Functional Lead       | Heinrich Lee Yu       | Senior Backend Engineer, Plan  |
| Functional Lead       | Gabe Weaver           | Senior Product Manager, Plan   |
| Functional Lead       | Sean McGivern         | Staff Backend Engineer, Scalability   |
| Member                | Markus Koller         | Backend Engineer, Create       |
| Member                | Scott Stern           | Frontend Engineer, Plan        |
| Member                | Ben Kochie            | Site Reliability Engineer      |
| Member                | Natalia Tepluhina            | Knowledge      |
| | |
