---
layout: markdown_page
title: "Category Direction - Runner"
---

- TOC
{:toc}

## GitLab Runner

GitLab Runner is the multi-platform execution agent that works with [GitLab CI](/direction/verify/continuous_integration)
to execute the jobs in your pipelines.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARunner)
- [Overall Vision](/direction/ops#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## What's Next & Why

### Multi-Platform Support

- [Autoscaling CI on AWS Fargate:](https://gitlab.com/gitlab-org/gitlab-runner/issues/2972) While customers have successfully implemented Autoscaling GitLab Runner on AWS EC2 instances, there has been demand from customers who need to take advantage of "serverless compute engine" solutions such as Fargate that further reduces the infrastructure management burden of managing CI at scale. Autoscaling GitLab CI on an AWS Elastic Container Service (ECS) cluster managed by AWS Fargate is a new autoscaling solution where the GitLab Runner will automatically launch a container to execute each GitLab CI job.
- [ARM64 Docker Image](https://gitlab.com/gitlab-org/gitlab-runner/issues/2076) Another highly requested capability is support for ARM64. So, we are actively working on expanding our support for ARM64 by introducing an ARM64 GitLab Runner container image.
- [Mac Shared Runners:](https://gitlab.com/groups/gitlab-org/-/epics/1830) Today, on GitLab.com, we provide Shared Runners on Linux and Windows virtual machines. Next up will be the addition of Mac Shared Runners. These Mac Shared Runners will give the macOS community the infrastructure to build their projects on GitLab.com without needing to setup and maintain build machines.


### Make CI Easy for Self-Managed 

- [Make CI easy for Self-Managed GitLab users:](https://gitlab.com/groups/gitlab-org/-/epics/2778) Our goal is that users self-managed GitLab should be able to get up and running with CI with as little friction as possible.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)),
we do not have an upcoming target for bringing it to the next level. However, it's important to us that
we defend our lovable status and provide the most value to our user community.
To that end, we are starting to work on collecting community feedback to understand users' pain points with installing, managing, and using GitLab Runners. 
If you have any feedback that you would like to share, you can do so in this [epic](https://gitlab.com/gitlab-org/gitlab/issues/39281). 

Maturing the Windows executor and autoscaler is of particular importance as we improve our support for Windows
across the board. There are a few issues we've identified as being part of that effort:

- [Create VMs in background to speed-up the autoscaling](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/issues/32)
- [Distribute Docker windows image for gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner/issues/3914)
- [Support named pipes for Windows Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/4295)
- [Use Docker Save to publish docker helper images](https://gitlab.com/gitlab-org/gitlab-runner/issues/3979)
- [Add support for Windows device for Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/3923)

## Competitive Landscape

The Runner is currently evaluated as part of the comprehensive competitive analysis in the [Continuous Integration category](/direction/verify/continuous_integration/#competitive-landscape)

## Top Customer Success/Sales Issue(s)

For the CS team, the issue [gitlab-runner#3121](https://gitlab.com/gitlab-org/gitlab-runner/issues/3121) where orphaned
processes can cause issues with the runner in certain cases has been highlighted as generating
support issues.

## Top Customer Issue(s)

The top requested customer issue that we are investigating is [gitlab-runner#2797](https://gitlab.com/gitlab-org/gitlab-runner/issues/2797). 
The principal goal of this feature request is to enable the execution of a GitLab pipeline on a users' local system so that a developer is able to develop, test, and validate their CI pipelines quickly. 

Other popular issues include:

- [gitlab-runner#1809](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809): Use variable inside other variable
- [gitlab-runner#1057](https://gitlab.com/gitlab-org/gitlab-runner/issues/1057): Specify root folders for artifacts
- [gitlab-runner#6400](https://gitlab.com/gitlab-org/gitlab-runner/issues/6400): Make environment variables set in before_script available for expanding in .gitlab-ci.yml
- [gitlab-runner#1107](https://gitlab.com/gitlab-org/gitlab-runner/issues/1107): Docker Artifact caching
- [gitlab-runner#2972](https://gitlab.com/gitlab-org/gitlab-runner/issues/2972): Autoscaling GitLab Runner on AWS Fargate
- [gitlab-runner#3392](https://gitlab.com/gitlab-org/gitlab-runner/issues/3392): Multi-line command output can be un-collapsed in Job terminal output view

## Top Internal Customer Issue(s)

There are a few top internal customer issues that we're investigating :

- [gitlab#16319](https://gitlab.com/gitlab-org/gitlab/-/issues/16319): Allow registration of runners via API
- [gitlab#24674](https://gitlab.com/gitlab-org/gitlab/-/issues/24674): Backstage work to enable interactive web terminals on shared runners
- [gitlab#25969](https://gitlab.com/gitlab-org/gitlab/issues/25969): Allow advanced configuration of GitLab runner when installing GitLab managed apps

## Top Vision Item(s)

### Offer Premium Machine Types in Shared Runner Fleet

We currently offer a single size for runners in our shared fleet, but some jobs take more or less CPUs, and some take more or less RAM. Offering additional options will help users who are not comfortable with or not interested in running their own runners.

To follow along, or add feedback to this critical topic, refer to [Epic #2426](https://gitlab.com/groups/gitlab-org/-/epics/2426)

### Migrating from Docker Machine for Runner Autoscaling

Autoscaling of GitLab Runner on Virtual Machines hosted on the major cloud platforms is done today with Docker Machine which is in [maintenance mode](https://forum.gitlab.com/t/docker-machine-is-now-in-maintenance-mode/29381). As such, a key strategic initiative is migrating away from Docker Machine for autoscaling. To follow along, or add feedback to this critical topic, please review the epic [gitlab-org&2502](https://gitlab.com/groups/gitlab-org/-/epics/2502).

### Shared runner billing and management for self-managed

We are also exploring enabling users of self-managed GitLab instances (CE and EE) to purchase CI minutes, [gitlab-org&835](https://gitlab.com/groups/gitlab-org/-/epics/835). This solution will also include management and reporting capability so that users can see a clear history of Runner minutes granted and consumed.

### Additional Platforms

A common request we get is to add support for various platforms to the Runner. We've chosen to support
the plethora of different systems out there by adding support for a [custom executor](https://docs.gitlab.com/runner/executors/custom.html) that can be overridden to support different needs. In this way platforms like [Lambda](https://gitlab.com/gitlab-org/gitlab-runner/issues/3128), [vSphere](https://gitlab.com/gitlab-org/gitlab-runner/issues/2122), and
even custom in-house implementations can be implemented.

Other platforms that we are investigating adding support for via the custom executor include [Google Cloud Run](https://gitlab.com/gitlab-org/gitlab-runner/issues/5028).
There is also high community interest from customers who have IBM z/OS Mainframe's and want to use GitLab Runners on that platform. Our goal for an MVC, [z/OS Mainframe support for GitLab CI Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3263), is to release a docker image with GitLab Runner for Linux on IBM Z. 

Additional platforms are primarily being implemented through community contributions, so if you're interested in contributing to GitLab these issues are great ones to get involved with.
