---
layout: handbook-page-toc
title: "Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
----

NOTE: Our contractor agreements and employment contracts are all on the [Contracts](/handbook/contracts/) page.

## Entity Specific Benefits
- [GitLab BV (Netherlands)](/handbook/benefits/bv-benefits-netherlands)
- [GitLab BV (Belgium)](/handbook/benefits/bv-benefits-belgium)
- [GitLab Lyra (India)](/handbook/benefits/lyra-benefits-india)
- [GitLab Inc (US)](/handbook/benefits/inc-benefits-us)
- [GitLab Inc (China)](/handbook/benefits/inc-benefits-china)
- [GitLab LTD (UK)](/handbook/benefits/ltd-benefits-uk)
- [GitLab GmbH (Germany)](/handbook/benefits/gmbh-benefits-germany)
- [GitLab PTY (Australia)](/handbook/benefits/pty-benefits-australia)
- [GitLab Canada Corp](/handbook/benefits/canada-corp-benefits)
- [Safeguard (Ireland, Hungary and Spain)](/handbook/benefits/safeguard/)
- [CXC (New Zealand)](/handbook/benefits/cxc/)
- Contractors of GitLab BV are eligible for the [general benefits](/handbook/benefits/#general-benefits), but are not eligible for entity specific benefits as they have an additional 17% added to the [compensation calculator](/handbook/people-group/global-compensation/#compensation-calculator). A contractor may bear the costs of their own health insurance, social security taxes and so forth, leading to a 17% higher compensation for the contractor within the calculator.

## Guiding Principles

These principles will guide our benefit strategies and decisions.

* **Collaboration**
  - Work with providers, vendors, and global networks to benchmark benefit data from other similar sized companies.
  - Foster cross-company understanding.
* **Results**
  - Evangelize benefit programs in each entity.
  - Transform “statutory” to “competitive.”
  - Make benefits a very real aspect of compensation during the hiring process.
  - Measure employee engagement and benefit enrollment.
* **Efficiency**
  - Use the GitLab Inc (US) benefits as a starting point, and try to stay as consistent as possible within the US baseline.
  - Iterate on changes for specific countries based on common practices and statutory regulations.
* **Diversity & Inclusion**
  - Actively work to ensure that our benefit choices are inclusive of all team members.
* **Iteration**
  - Regularly review current benefits (bi-annually) and propose changes.
  - Everything is always in draft.
* **Transparency**
  - Announce and document benefits to keep them updated.
  - Share upcoming benefit plans internally before implementing them.
  - Invite discussion and feedback.

We value opinions but ultimately People Operations/Leadership will make the decision based on expert advice and data.

### Guiding Principles in Practice

When establishing a new co-employer or entity we will outline the following benefits as to why it is or is not offered.

1. Medical
1. Pension
1. Life Insurance

We do not have specific budgets around benefit costs, but instead look to increasing the ability to recruit and retain team members in favorable locations. We do not take a global approach to offering the same benefits in all countries, but will transparently outline why we do or do not offer the above benefits on their respective [entity specific benefits](/handbook/benefits/#entity-specific-benefits) page.

## General Benefits

For the avoidance of doubt, the benefits listed below in the General Benefits section are available to contractors and employees, unless otherwise stated. Other benefits are listed by countries that GitLab has established an entity or co-employer and therefore are applicable to employees in those countries only. GitLab has also made provisions for Parental Leave which may apply to employees and contractors but this may vary depending on local country laws. If you are unsure please reach out to the Total Rewards team.

1. GitLab will pay for the items listed under [spending company money](/handbook/spending-company-money).
1. [Stock options](/handbook/stock-options/) are offered to most GitLab team members.
1.  Deceased team member:
    In the unfortunate event that a GitLab team member passes away, GitLab will
    provide a [$20,000](/handbook/people-group/global-compensation/#exchange-rates) lump sum to anyone of their choosing. This can be a spouse,
    partner, family member, friend, or charity.
      * For US based employees of GitLab Inc., this benefit is replaced by the
        [Basic Life Insurance](/handbook/benefits/inc-benefits-us/#basic-life-insurance-and-add).
      * For all other GitLab team members, the following conditions apply:
         * The team member must be either an employee or direct contractor.
         * The team member must have indicated in writing to whom the money
           should be transferred. To do this you must complete the [Expression of wishes](https://docs.google.com/document/d/1bBX6Mn5JhYuQpCXgM4mkx1BbTit59l0hD2WQiY7Or9E/edit?usp=sharing) form. To do this, first copy the template to your Google Drive (File -> Make a copy), enter your information, sign electronically. To sign the document, use a free document signing program like [smallpdf](https://smallpdf.com/sign-pdf) or [HelloSign](https://app.hellosign.com/); or you can print it, sign and digitize. Sign, save as a pdf and upload to your Employee Uploads folder in BambooHR.
         * For part-time GitLab team members, the lump sum is calculated pro-rata, so
           for example for a team member that works for GitLab 50% of the time,
           the lump sum would be [$10,000](/handbook/people-group/global-compensation/#exchange-rates).
1. [Paid time off policy](/handbook/paid-time-off)
1. [Tuition Reimbursement](/handbook/people-group/code-of-conduct/#tuition-reimbursement)
1. [GitLab Contribute](/company/culture/contribute)
   * Every nine months or so GitLab team members gather at an exciting new location to [stay connected](/blog/2016/12/05/how-we-stay-connected-as-a-remote-company/), at what we like to call GitLab Contribute. It is important to spend time face to face to get to know your team and, if possible, meet everyone who has also bought into the company vision. There are fun activities planned by our GitLab Contribute Experts, work time, and presentations from different functional groups to make this an experience that you are unlikely to forget! Attendance is optional, but encouraged. For more information and compilations of our past events check out our [previous Contributes (formerly called GitLab Summit)](/company/culture/contribute/previous).
1. [Business Travel Accident Policy](https://drive.google.com/a/gitlab.com/file/d/0B4eFM43gu7VPVl9rYW4tXzIyeUlMR0hidWIzNk1sZjJyLUhB/view?usp=sharing)
   * This policy provides coverage for team members who travel domestic and internationally for business purposes. This policy will provide Emergency Medical and Life Insurance coverage should an emergency happen while you are traveling. In accompaniment, there is coverage for security evacuations, as well a travel assistance line which helps with pre-trip planning and finding contracted facilities worldwide.
   * Coverage:
      - Accidental Death [enhanced coverage]: 5 times Annual Salary up to USD 500,000.
      - Out of Country Emergency Medical: Coverage up to $250,000 per occurrence. If there is an injury or sickness while outside of his or her own country that requires treatment by a physician.
      - Security Evacuation with Natural Disaster: If an occurrence takes place outside of his or her home country and Security Evacuation is required, you will be transported to the nearest place of safety.
      - Personal Deviation: Coverage above is extended if personal travel is added on to a business trip. Coverage will be provided for 25% of length of the business trip.
      - Trip Duration: Coverage provided for trips less than 180 days.
      - Baggage & Personal Effects Benefit: $500 lost bag coverage up to 5 bags.
   * For any assistance with claims or questions, please contact the [Total Rewards Analyst].
   * This policy will not work in conjunction with another personal accident policy as the Business Travel Accident Policy will be viewed as primary and will pay first.
   * For more detailed information on this benefit, please reference the [policy document](https://drive.google.com/file/d/1piK9ch6lBrR44DzvtF9GObQZC0N5dTaZ/view?usp=sharing).
1. [Immigration](/handbook/people-group/visas/) Benefits for eligible team members.
1. [Employee Assistance Program](/handbook/benefits/#employee-assistance-program)
1. [Incentives](/handbook/incentives) such as
   - [Sales Target Dinner Evangelism Reward](/handbook/incentives/#sales-target-dinner)
   - [Discretionary Bonuses](/handbook/incentives/#discretionary-bonuses)
   - [Referral Bonuses](/handbook/incentives/#referral-bonuses)
   - [Visiting Grant](/handbook/incentives/#visiting-grant)

## All-Remote

GitLab is an [all-remote](/blog/2018/10/18/the-case-for-all-remote-companies/) company; you are welcome to [read our stories](/company/culture/all-remote/stories/) about how working remotely has changed our lives for the better.

You can find more details on the [All Remote](/company/culture/all-remote/) page of our handbook.

_If you are already a GitLab employee and would like to share your story, simply add a `remote_story:` element to your entry in `team.yml` and it will appear
on that page._

## Part-time contracts

As part of our Diversity & Inclusion value, we support [Family and friends first](/handbook/values/#family-and-friends-first-work-second) approach. This is one of the many reasons we offer part-time contracts in some teams.

We are growing fast and unfortunately, not all teams are able to hire part-time employees yet. There are certain positions where we can only hire full-time employees.

Candidates can ask for part-time contract during the interview process. Even when a team they are interviewing for can't accept part-time employees, there might be other teams looking for the same expertise that might do so. If you are a current team member and would like to switch to a part-time contract, talk to your manager first.

## Parental Leave

Anyone (regardless of gender) at GitLab who becomes a parent through childbirth, adoption, or foster care is able to take fully paid parental leave. GitLab team members will be encouraged to decide for themselves the appropriate amount of time to take and how to take it. We will offer everyone who has been at GitLab for a year up to 16 weeks of 100% paid time off during the first year of parenthood. We encourage parents to take the time they need.

For many reasons, a team member may require more time off for parental leave. Many GitLab  members are from countries that have longer standard parental leaves, occasionally births have complications, and sometimes 16 weeks just isn't enough. Any GitLab team member can request additional unpaid parental leave, up to 4 weeks. We are happy to address anyone with additional leave requests on a one-on-one basis. All of the parental leave should be taken in the first year.

If you have been at GitLab for a year your parental leave is fully paid. If you've been at GitLab for less than a year it depends on your jurisdiction. If applicable, commissions are paid while on parental leave based on the prior six months of performance with a cap at 100% of plan. For example, if in the six months prior to starting parental leave you attained 85% of plan, you will be compensated at the same rate while on leave. On the day you return from leave and going forward, your commissions will be based on current performance only.

You are entitled to and need to comply with your local regulations. They override our policy.

To [alleviate the stress](/handbook/paid-time-off/#returning-from-work-after-parental-leave) associated with returning to work after parental leave, GitLab supports team members coming back at [50% capacity](/handbook/paid-time-off/#returning-to-work-at-50-capacity). Parents at GitLab who are reentering work following parental leave are encouraged to reach out to team members who self-designate as a [Parental Leave Reentry Buddy](/handbook/general-onboarding/parental-leave-buddy/).

Managers of soon to be parents should check out this [Parental Leave Manager Tool Kit](https://about.gitlab.com/handbook/benefits/parental-leave-buddy/#manager-tool-kit) for best practices in supporting your team members as they prepare for and return from Parental Leave.

**How to Apply for Parental Leave**

1.  To initiate your parental leave, please send an email to Total Rewards Analysts (`total-rewards@gitlab.com`) and your manager at least thirty days before your leave will begin, and also take a look at the process for application based on your employment company Country Specific Leave Requirements, which are subject to change as legal requirements change:

    * [GitLab Inc. United States Leave Policy](/handbook/benefits/inc-benefits-us/#gitlab-inc-united-states-leave-policy)
    * [GitLab Inc. China Leave Policy](/handbook/benefits/inc-benefits-china/#gitlab-inc-china-leave-policy)
    * [GitLab B.V. Netherlands Leave Policy](/handbook/benefits/bv-benefits-netherlands/#gitlab-bv-netherlands-leave-policy)
    * [GitLab B.V. Belgium Leave Policy](/handbook/benefits/bv-benefits-belgium/#gitlab-bv-belgium-leave-policy)
    * [GitLab B.V. India Leave Policy](/handbook/benefits/lyra-benefits-india/#gitlab-bv-india-leave-policy)
    * [GitLab LTD United Kingdom Leave Policy](/handbook/benefits/ltd-benefits-uk/#gitlab-ltd-united-kingdom-leave-policy)
    * [GitLab GmbH Germany Leave Policy](/handbook/benefits/gmbh-benefits-germany/#gitlab-gmbh-germany-leave-policy)
    * [GitLab PTY Australia Leave Policy](/handbook/benefits/pty-benefits-australia/#gitlab-pty-australia-leave-policy)

    If you need any additional leave, please refer to our [Paid Time Off Policy](/handbook/paid-time-off).

    If you're interested in learning about how other GitLab team members approach parenthood, take a look at [the parenting resources wiki page](https://gitlab.com/gitlab-com/gitlab-team-member-resources/wikis/parenting) and [#intheparenthood](https://gitlab.slack.com/messages/CHADS8G12/) on Slack.

2. Process for Total Rewards Analysts:
  * Log and monitor upcoming parental leave in the "Parental Leave" Google sheet on the drive.
  * Once the leave is official:
    * File all documentation in BambooHR.
    * Update the Employment Status of the team member to "Parental Leave" with the date the team member started leave.
  * Add another entry with the date the team member will return from leave with the status "End of Parental Leave" and in the comment section, add "Tentative". This will generate a notification to the Compensation & Benefits team 1 day after the team member returns from leave.
  * Notify payroll.
  * Once BambooHR notification is received:
    * Change "End of Parental Leave" status to "Active" and in the comment section, add "Return from leave".
    * Update payroll of the team's return date.

## COVID-19 Medical Leave Policy

GitLab has long believed in ["family and friends first”](/handbook/values/#family-and-friends-first-work-second) and we realize that our team members may require some additional time away from work in the event they or a dependent family member are directly affected by COVID-19. In order to best support our team members who find themselves or family member(s) directly affected by COVID-19, GitLab has created this COVID-19 Medical Leave Policy to allow for leave time explicitly and guaranteed. 

GitLab team members directly affected by COVID-19 who need to take leave for one of the two reasons set forth below are eligible to take up to 12 weeks of leave with full pay after utilizing our Paid Time Off policy. In the event there are differences between the sick leave provided under your local law and this GitLab COVID-19 Policy, your local law will supersede this policy.
1. The team member has received a confirmed medical diagnosis of COVID-19 from a licensed healthcare provider. 
1. The team member is caring for a [dependent family member](/handbook/benefits/#faq-for-covid-19-medical-leave-policy) who has received a confirmed medical diagnosis of COVID-19 from a licensed healthcare provider. 

You do not have to physically see your physician for this confirmation. If telehealth services are available, we highly encourage you to utilize those in line with your community’s regulations around physical distancing, quarantine and self isolation.

The ability to request COVID-19 Leave will run through April 30, 2020. That means, any requests for COVID-19 Leave must be made prior to April 30, 2020, however the leave itself may exceed that timeframe. By way of an example, if a team member requests COVID-19 Leave on April 29, 2020 for 4 weeks, the end date for their COVID-19 Leave would be May 27, 2020. 

As the situation around COVID-19 continues to evolve, GitLab will revisit this policy and may adjust it accordingly. 

### How to apply for COVID-19 Leave:

We encourage team members to only take the time they need to recover in accordance with the recommendation of your licensed healthcare provider. The maximum amount of time posted under this COVID-19 Leave Policy is 12 weeks, but medically, you may need less. 

For some, our [Paid Time Off Policy](/handbook/paid-time-off/) of 25 consecutive calendar days will be suitable. Please keep your manager informed of any upcoming Paid Time Off. You are in no way obligated to release a personal medical diagnosis of COVID-19 to your manager. 

If you meet the criteria set forth above and find that you need additional time past the 25 consecutive calendar days allowed under our Paid Time Off Policy, please follow these steps to apply for COVID-19 Leave:
* On or before day 20 of your Paid Time Off,  please email total-rewards@gitlab.com with the following to request COVID-19 Leave: 
  * Start date of your Paid Time Off 
  * Start date of your COVID-19 Leave (this will be the 26th day after your Paid Time Off began)
  * Anticipated end date of your COVID-19 Leave
  * A note from a licensed healthcare professional confirming the leave for yourself or a dependent family member which you will be caring for and the start and end dates of your COVID-19 Leave (which should be consistent with the anticipated dates you are providing within your request). The Total Rewards team will not ask for test results. 
* The Total Rewards team will review the documents provided and communicate directly with the team member regarding any additional information needed (if necessary) and the result.
* If approved, the Total Rewards team will notify the PBP of the group and manager of the start and end dates of the COVID-19 Leave. The Total Rewards team will not specify to your PBP or manager the reason for the leave. 
* The Total Rewards team will notify payroll of the leave dates. 
* Stock Vesting, bonus payments, and benefits will not be affected by this leave during the 12 week time period. 
* The Total Rewards team will follow up with the team member on a weekly basis to see how the team member is doing. If needed and in accordance with applicable law, the Total Rewards team will request additional documentation around inability to work due to COVID-19.
* The Total Rewards team will confidentially file all ongoing documentation of inability to work through the weekly check ins. 

### FAQ for COVID-19 Medical Leave Policy 

* Does this leave cover leave to care for children under the age of 18 who are home because their school or childcare is unavailable due to COVID-19?
  * No. We are reviewing the various legislation being passed in different jurisdictions and determining whether other forms of leave may be necessary to accommodate. 
* Can the leave be intermittent (e.g., 4 hours/day or 3 days/week)
  * No. If, as this situation evolves and it is determined that recovery may allow work on an intermittent basis, the Total Rewards team will review and consider adjusting the policy as needed.
* Will this policy apply across all of GitLab or just U.S.?  
  * This policy is intended to apply globally.  Local laws or internal policies governing local jurisdictions that provide broader protections for team members will supersede this policy.
* How does this leave interact with other leave policies provided by GitLab?
  * As previously noted, local leave laws or policies will supersede this policy.  Additionally, the Military Leave policy will govern military specific leave needs. 
* How does this policy work in conjunction with my 25 days of unlimited PTO?
  * This policy grants up to twelve weeks additional leave after you have used your initial 25 days PTO.  For many people, you will not need to use all of the 12 weeks to recover.
* What if I can’t get tested for COVID-19?
  * The Total Rewards team needs a note from your health care provider certifying that you cannot work because you or a dependent family member you care for has been diagnosed (either via test or presumptively) with COVID-19.  The Total Rewards team does not require you to provide testing results. You do not have to physically see your physician for this confirmation. If telehealth services are available, we highly encourage you to utilize those in line with your community’s regulations around social distancing and self isolation.
* Will anyone at GitLab know that I am sick? How will my privacy be respected?
  * The Total Rewards team will maintain your privacy and keep any documentation private (it will not be shared with your manager or anyone on your team) and only shared within the Total Rewards team as needed to process your leave. Your manager and your team will have to be informed that you are on leave as you will not be available for work as normal, but will not provide the reason for the leave.
* What is a “dependent family member?”
  * A dependent family member is someone who you are directly responsible for as a caregiver. Examples can be: a spouse or significant other, dependent child; parent, sibling, etc
* What if I am on a commission plan?
  * If applicable, commissions are paid according to your applicable commission plan while on COVID-19 related leave based on the prior six months of performance with a cap at 100% of plan. On the day you return from leave and going forward, your commissions will be based on current performance only.
* What is the last day to apply for COVID-19 Leave? 
  * The ability to request COVID-19 Leave will run through April 30, 2020. That means, any requests for COVID-19 Leave must be made prior to April 30, 2020, however the leave itself may exceed that timeframe. By way of an example, if a team member requests COVID-19 Leave on April 29, 2020 for 4 weeks, the end date for their COVID-19 Leave would be May 27, 2020. As the situation around COVID-19 continues to evolve, GitLab will revisit this policy and may adjust it accordingly. 

## Employee Assistance Program

GitLab offers an an Employee Assistance Program to all team members via [Modern Health](https://www.joinmodernhealth.com/). Modern Health provides technology and professional support to help reduce stress, feel more engaged, and be happier. Through GitLab, you have access to coaching sessions at no cost to you.

### What does Modern Health offer?

Modern Health is the one-stop shop for all tools related to mental well-being and self-improvement. Members gain access to the following features:
* **Personalized Plan:** Take a well-being assessment and review which tools may be most helpful for you.
* **Professional Support** Get matched to a dedicated coach or therapist who can help you reach your personal and professional goals. Coaching visits are covered by GitLab and are offered at no cost to you.
* **Curated Content Library:** Learn quick tips and tricks to prevent burnout, manage stress, and cope with anxiety or depression based on our evidence-based digital programs. You can use these digital programs for in-the-moment support or to build your own toolkit of techniques to improve your mental well-being.

If you’re still not sure where to get started, we recommend that you:
1. Download the app and register your account
2. Register and take the well-being assessment and
1. Get matched to a dedicated coach who can work with you to figure out next steps.

### Which areas does Modern Health support?

Modern Health cultivates the resilience needed to weather the ups and downs of everyday life. Here are the specific areas where we can help:
* Work Performance: Productivity, Leadership Skills, Work Relationships, Professional Development
* Relationships: Romantic Relationships & Dating, Family, Friends, Breakups
* Stress & Anxiety: Anxiety, Depression, Stress, Resilience
* Healthy Lifestyles: Sleep, Physical Activity, Eating Well, Habits
* Financial Well-being: Goals, Budgeting Savings and Debt, Management, Investing
* Diversity & Inclusion: Gender, Equality, Unconscious Bias, LGBTQ
* Life Challenges: Pregnancy/Parenting, Elder/Child Care, Loss of a Loved One, Illness
* Mindfulness & Meditation: Stress Less, Sleep Better, Focus Better, Meditation for Beginners

Note: This list isn’t intended to be comprehensive. Please reach out to `help@joinmodernhealth.com` with any questions about how or where to get started.

### How does Modern Health think about mental health?

The philosophy towards mental health comes from the World Health Organization (WHO): “in which every individual realizes his or her own potential, can cope with the normal stresses of life, can work productively and fruitfully, and is able to make a contribution to her or his community.”

### When do my Modern Health benefits reset?

Your benefits reset 1 year from the launch date, on May 15th, 2020.

### What languages is Modern Health available in?

The platform is currently available in English and Spanish. If English or Spanish is not your preferred language, please note Care is provided in most languages. Feel free to email `help@joinmodernhealth.com` to find out more.

### Registration

* Am I eligible? All GitLab team members are eligible for Modern Health.
* How do I register?
  * Download the Modern Health app in the [Google Play Store (Android)](https://play.google.com/store/apps/details?id=com.modernhealth.modernhealth) or [App Store (iOS)](https://itunes.apple.com/us/app/modern-health/id1445843859?ls=1&mt=8).
  * After your download is complete, select “Join Now” from the welcome page of the mobile app.  
  * Use the first and last name you have on file with GitLab.
  * Verify using your company email.
  * Enter the your GitLab email and password of your choice. (Reminder that you must register with your GitLab email, but you can change your Modern Health account email to your personal email in Settings upon registration.)
  * Select “Register” on the web or “Agree & Join” on the mobile app to complete registration.

If you have trouble registering for Modern Health, please don’t hesitate to reach out to `help@joinmodernhealth.com` with a note or screenshot. Their customer support team will verify the information against what they have on file to provide you the best instructions on how to successfully access Modern Health.

### What is the well-being survey, and why should I take it?

Similar to an annual physical with your primary care physician, Modern Health’s well-being survey serves as a check up for your mental health. You can retake the survey to track your progress overtime.

Your well-being score empowers experts at Modern Health to provide you the best user experience. It enhances the customization of your personalized wellness plan, which makes it more effective in addressing your specific needs. Although the ups and downs in well-being score are inevitable, the data-driven approach keeps up with how you’re doing over time to support you with the tools to improve no matter where your score is at today.  

### Care

**What is coaching?**
Coaching is a collaborative process to help you make important changes in your personal and professional life. Your coach is there to help you figure out how you want to change and the steps you need to take to do so. Your coach’s job is to help you organize your thoughts, emotions, and goals and break things down into smaller steps that create forward movement and growth. The client is the driver of these sessions, while the coach is there to provide reflection, clarity, and accountability.

**What is the difference between coaching and therapy?**
Modern Health’s belief is that anyone can benefit from working with a coach, and some people need therapy in addition to or instead of coaching. The primary difference between coaching and therapy is that therapy is conducted by licensed mental health professionals who are trained to treat clinical difficulties (e.g., depression, anxiety) whereas coaches work on non-clinical issues (e.g, personal growth, financial well-being, and professional development). If you are experiencing a clinical need, please access therapy through your health insurance or EAP. You can also work with your dedicated coach to determine if you would benefit from therapy.

**How do you match me to a coach?**
Modern Health matches you to appropriate coach based on a proprietary algorithm that takes into account your well-being assessment and areas you want to focus on.

**How often should I meet with my coach?**
How often you meet with your coach depends on your personal situation. Some people like to meet weekly, whereas others meet every month or two.

**Can I complete sessions with my partner or a family member?**
Yes, you are able to use your sessions with your partner or a family member. If you choose to do so, each completed session will count as one of your covered sessions.  Please discuss this directly with your coach.

**What can I expect during my first coaching session?**
During the first session, your coach will kick things off with an introduction to what coaching is and what you can expect during the session, including reviewing confidentiality (nothing you discuss is shared with Modern Health or your employer unless you request more support or need crisis resources). They will then ask you a few questions to better understand what you hope to get out of the coaching experience and what a successful experience would be for you. By setting expectations up front, the coach will be able to better help you identify personal goals and take steps towards achieving them. In subsequent sessions, the coach will follow up on any action items to understand their impact and you will together come up with next steps. The standard coaching session is 30 minutes and takes place over video, usually every two weeks or once a month.  In between sessions, you are encouraged to reach out to your coach with any follow up questions or advice via Modern Health’s digital messaging tool in the app as well as utilize the digital CBT and meditation programs.

**How should I prepare for my first session?**
Before your first session, we encourage you to ask yourself a few questions. As with many other aspects of your life, the more effort you put into your coaching experience, the more you’ll get out of it. These can help guide your initial conversations with your coach.
* Where do you want to start?
* You might have a lot of areas of interest (e.g., you want to learn mindfulness, and work on your finances). Spend some time thinking about where you want to get started before your first visit. What is most on your mind or stressing you out?
* Do you know what your goals are or do you need help figuring that out?
* What do you want to get out of coaching?
* What do you want your coach to be like? Do you want someone who holds you accountable, who is a cheerleader, who challenges you?
* How do you want your life to look when you are done? What does success look like?

**What happens if my provider isn’t a good fit?**
Our goal at Modern Health is to find someone that you feel you can do good work with and who can do good work with you! If you think the first person you meet with doesn’t seem like a good fit, just let the Modern Health team know (help@joinmodernhealth.com) and they will connect you with someone new.

### Logistics

* You have unlimited access to your coach over text and/or email.
* Coaching sessions are held over video/phone.
* If you need to miss a scheduled session please let your coach know at least 24 hours before your appointment. If you cancel after that time, or miss the session, it will count towards your total covered sessions.

**What do I do if I need support in between sessions?**
Beyond sessions, your coach is available through chat to check in on your progress toward your goals, as well as provide ad hoc support when situations come up that you might need advice on, but don’t need a full session for. To chat with your coach, click the blue text bubble on the bottom center of the mobile app.

Note: Modern Health is not a crisis resource. If you are experiencing a mental health emergency, please go to the nearest emergency room or contact a local emergency response line. You can find local and international resources by selecting “Information” on the bottom right of your mobile app, and then clicking the red “Access to 24/7 Crisis Information” banner at the top of the screen.

### Confidentiality

**Does my employer know if I’m using Modern Health?**
All information submitted through the Modern Health application is kept confidential and used to deliver a more personalized experience. No individual usage data will ever be shared back with your employer.

**Is what I discuss with my coach confidential?**
All information between you and your coach is confidential, except in the following cases:
* You are at risk of harming themselves and/or others
* Child, elder adult, or dependent adult abuse
* Court subpoenas

**How do you protect my information?**
Hypertext Transfer Protocol Secure (HTTPS) encryption measures for all data exchanged between our members and the application. Conversations are encrypted in transit via SSL (TLS v1.2) and each conversation (between a member and a provider) has its own encryption key which is stored in a separate, secure management system. Message contents are encrypted upon receipt by the web server and are transported and stored in internal systems.

### Crisis Support

**What do I do in a crisis?**
If you are experiencing a crisis (e.g., thoughts about suicide, thoughts about harming yourself or others, medical crisis, or in a dangerous situation) please call:
* emergency responders (911 or the [appropriate local emergency number for your area](https://en.wikipedia.org/wiki/List_of_emergency_telephone_numbers))
* in the US/Canada, https://suicidepreventionlifeline.org/ or 1-800-273-8255) or [a crisis line in your area](https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines)
* head to the nearest emergency room.

### Modern Health Weekly Reporting

* Pull the Modern Health BambooHR report
* Sort by hire date, remove anyone who hasn't started and copy the report
* Open the `Modern Health Upload` spreadsheet on the google drive and paste the report under a new tab
* Rename the tab to today's date
* Download as a CSV file
* Email the file the secure upload using Box


## Global Benefits Survey

We've partnered with Culture Amp to run our Global Benefits Survey, which will launch on June 10th. We are running this survey to collect feedback from all team members, which will be collected, aggregated, and analyzed. Using Culture Amp's reporting tools, we'll analyze the data to find the most common trends for desired benefit iterations. We will use this information to generate a roadmap and share these results with everyone at GitLab.

This is an opportunity to share with the Total Rewards team what benefits you think are great, what benefits are lacking, and what you think is most important to adjust. While we will review and prioritize all feedback, not all input may be implemented in the next few iterations. We will continue to ask the organization what is important to team members, but also take into account our fiduciary responsibility to maintain our operating costs.

**Timeline of the Survey Results:**
* 2019-06-10 Survey goes out to team members: DONE
* 2019-06-21 Survey closes: DONE
* 2019-06-24 to 2019-07-05 Survey results analyzed: DONE
* 2019-07-12 Survey Results Shared with the GitLab Team: DONE
* 2019-07-15 to 2019-07-26 Survey Results issues opened to begin implementation of any changes.
* Q3 OKR to implement changes based on survey results viewed in Q2.

Thank you again for playing a part in our efforts to continually improve total rewards at GitLab. If you have any questions, please reach out to the Total Rewards team via email.

### Global Benefits Survey Results

Participation: 62%

**Rating Rubric**
5 - strongly agree
4 - agree
3 - neutral
2 - disagree
1 - strongly disagree

**Rating Rubric Results:**

1. I Understand the Benefits Package
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 4.16  |
    | Female                 | 4.21  |
    | Individual Contributor | 4.13  |
    | Manager                | 4.35  |
    | Leader                 | 4.46  |
    | Company Overall        | 4.18  |
1. The general benefits at GitLab are equal to or better than what is offered by similar employers
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.67  |
    | Female                 | 3.66  |
    | Individual Contributor | 3.72  |
    | Manager                | 3.33  |
    | Leader                 | 3.46  |
    | Company Overall        | 3.66  |
1. I believe my benefits package is equal to or better than what is offered by similar employers
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.44  |
    | Female                 | 3.50  |
    | Individual Contributor | 3.54  |
    | Manager                | 3.00  |
    | Leader                 | 3.27  |
    | Company Overall        | 3.46  |
1. The general benefits at GitLab save me a great deal of time and/or money, and add significant value to my employee experience
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.75  |
    | Female                 | 3.79  |
    | Individual Contributor | 3.83  |
    | Manager                | 3.38  |
    | Leader                 | 3.54  |
    | Company Overall        | 3.77  |
1. My benefits package provides quality coverage for myself and, if applicable, my dependents
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.59  |
    | Female                 | 3.63  |
    | Individual Contributor | 3.63  |
    | Manager                | 3.38  |
    | Leader                 | 3.58  |
    | Company Overall        | 3.60  |
1. The wellness offerings at GitLab help me lead a happier, healthier life
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.39  |
    | Female                 | 3.28  |
    | Individual Contributor | 3.41  |
    | Manager                | 3.26  |
    | Leader                 | 2.92  |
    | Company Overall        | 3.36  |
1. I believe our benefits package is one of the top reasons why people apply
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 2.87  |
    | Female                 | 2.81  |
    | Individual Contributor | 2.96  |
    | Manager                | 2.40  |
    | Leader                 | 2.35  |
    | Company Overall        | 2.86  |
1. Should I have or care for a(nother) child, the parental leave policy is sufficient	 
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.95  |
    | Female                 | 3.44  |
    | Individual Contributor | 3.82  |
    | Manager                | 3.61  |
    | Leader                 | 4.04  |
    | Company Overall        | 3.81  |
1. The vacation policy allows me sufficient time to recharge
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 4.48  |
    | Female                 | 4.40  |
    | Individual Contributor | 4.49  |
    | Manager                | 4.27  |
    | Leader                 | 4.38  |
    | Company Overall        | 4.46  |
1. I believe our benefits package is one of the top reasons why people stay at GitLab
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 3.08  |
    | Female                 | 3.13  |
    | Individual Contributor | 3.21  |
    | Manager                | 2.58  |
    | Leader                 | 2.54  |
    | Company Overall        | 3.09  |
1. I believe investing more of the company's money into improving benefits will help attract and retain top talent
  * | GitLab                 | Score |
    |------------------------|-------|
    | Male	                 | 4.16  |
    | Female                 | 4.25  |
    | Individual Contributor | 4.20  |
    | Manager                | 4.25  |
    | Leader                 | 3.85  |
    | Company Overall        | 4.18  |

**Comment Responses:**

Please note this is a summary of suggestions based on aggregated global team member feedback, but are not guaranteed to be implemented or adjusted at GitLab.

1. What provider for benefits would you prefer? (Entity/PEO where we have data)
  * Australia: BUPA, HCF, NIB, AHM
  * Canada: Great West Life, Blue Cross, Manulife, Sunlife
  * India: ICICI Lombard, Apollo Munich, Bajaj Allianz
  * Ireland: VHI. Laya, Irish Life
  * Netherlands: UMC Zorgverzekering, VGZ, ABP
  * United Kingdom: Virgin Active, BUPA, AVIVA, AXA PPP
  * United States: 45% of respondents commented, 21% UHC, 14% BCBS, 3% Kaiser, 2% Cigna
1. What general benefit do you think would be the best new addition to the company's offering to implement?
  * Wellness program (gym, fitness, etc) (overwhelming support for this)
  * Global insurance programs to align with US/UK
  * Pension with company match globally
  * Company Annual Bonus or extra month pay for vacation bonus
  * Charitable Giving
  * Team Level Contribute
  * Other Benefits: Loan Assistance, Childcare, Pet Insurance, Food Allowance
1. What benefits would you want GitLab to retain?
  * Unlimited Paid Time Off
  * Parental Leave
  * Stock Options
  * Tuition Reimbursement
  * Contribute
  * Remote Work
  * Travel Stipend/Visiting Grant
  * Employee Assistance Program
  * All Benefits
1. What benefits would be most willing to sacrifice to allow for new benefits?
  * Sales Incentive Dinner
  * Stock Options
  * Employee Assistance Program
  * Tuition Reimbursement
  * Visiting Grant
  * Referral/Discretionary Bonus
  * Travel Accident Insurance

**Action Items/Issues to Open:**

Please note this is the first iteration/more immediate goals, but this section will be updated as we progress through the project.

* Clarify what Benefits Contractors are eligible for: [Merge Request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/26491/diffs)
* Ensure all PEO benefits are documented on the respective page: TODO
* Benchmark against other remote companies/survey data to prioritize new benefits to offer/replace: TODO
* Review Implementation of a Wellness Program (Gym/Fitness): [Issue](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/37)
* Review Global Health Benefits, Pension, Life Insurance: TODO
* Establish a benefits budget with Finance to implement new benefits: TODO
* Review all PEO benefits to ensure they align to our global benefits principles: TODO

## Benefits Not Currently Being Implemented

This section serves to highlight benefits that we have previously researched, but have decided not to move forward with implementing at this time. As the company grows, circumstances change, and/or we receive new information, we may decide to revisit any benefits added to this list.

**TeleHealth**

We researched and selected four vendors to receive more information. Demo calls were conducted with three of these vendors where we learned more about the solutions and pricing. After reviewing the [results of the benefits survey](/handbook/benefits/#global-benefits-survey-results), there wasn’t enough interest in a telehealth solution to justify the price so we decided to not move forward with any of the vendors at this time.

Further information and corresponding discussion are available under [Compensation Issue #15](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/15) (internal).

**Coworking Space Agreement**

We researched whether there was an account that could be set up with a coworking space chain that would make it easier for team members to utilize these spaces. Three major chains with global coverage were contacted with one responding. After conducting a survey, the type of account this chain offers doesn't align with the way the majority of team members utilize coworking spaces. Team members are still welcome to follow the existing process of [expensing a coworking space](/handbook/spending-company-money/).

Further information, survey results, and corresponding discussion are available under [Compensation Issue #30](https://gitlab.com/gitlab-com/people-ops/Compensation/issues/30) (internal).

**Wellness Program**

We researched and selected three vendors and conducted a cost analysis with Finance. At this time, it does not fit into the budget for benefits to be offered for FY21, but will be revisited when we evaluate benefits and budget for the next fiscal year. Further information and corresponding discussion are available under [Compensation Issue #37](https://gitlab.com/gitlab-com/people-group/Compensation/-/issues/37) (internal).

## Resources for COVID-19

GitLab is committed to the physical and mental wellbeing of all of our team members and wider community during the COVID-19 outbreak. We have put together this list to assist people in finding helpful resources when confronting different challenges during this time. This list is not exhaustive and contributions to it are encouraged. If you are experiencing difficulty breathing or any other life-threatening symptoms, please contact your country's emergency response system immediately.

### Simple Habit Free Trial

[Simple Habit](https://simplehabit.com/), a meditation and mental-wellness company, has offered all GitLab team members a free premium subscription through the month of April.

With this subscription, you will have unlimited access to Simple Habit's top wellness professionals, guided meditations, therapy, coaching, motivational talks and much more.

The code can be found in the following [shared doc](https://docs.google.com/document/d/1utRTqY0TkOK9He5zB3Iiia2ZQng5qoM9JzyBrV09P7Q/edit). 

### Modern Health Webinars

Modern Health is hosting a series of free Community Support Sessions designed to equip you with meaningful strategies to overcome stress or anxiety in various areas of your life caused by the COVID-19, such as managing isolation and loneliness, maintaining a healthy lifestyle, financial confidence, work-from-home wellbeing, and practicing mindfulness. 

In these sessions, coaches and therapists from our network will provide some advice on how to best approach these important areas of interest and then open up the floor to answer questions that you or the rest of the Modern Health community may have.

A full list of webinars taking place for the week and links to register can be found directly on [Modern Health's website](https://community.modernhealth.com/#For-Everyone).

For recordings of Sessions that have already taken place, please see [Modern Health's YouTube Channel](https://www.youtube.com/playlist?list=PLZoFVlbgk0-Nk-K8prckRd0vgJqT0ecmi).


### Global Resources

* Our [Employee Assistance Program](/handbook/benefits/#employee-assistance-program), Modern Health, is prepared to help with personalized coaching and therapy. They have a library of resources to help manage stress, cope with anxiety, and improve your mental well-being. 

### United States

* Our medical plans through UHC and Kaiser both have a variety of resources to help their members:
   * Kaiser:
     * For [video visits](https://healthy.kaiserpermanente.org/), please sign into your account. 
     * 24/7 Advice Line: 866-454-8855
     * Mail order prescriptions can be requested via the online portal or by calling the number at the top of your prescription label.
     * Emotional Support
       * [Wellness Resources](https://healthy.kaiserpermanente.org/northern-california/health-wellness/mental-health/tools-resources).
     * [Treatment Costs](https://healthy.kaiserpermanente.org/northern-california/health-wellness/coronavirus-information)
       * $0 out-of-pocket costs for COVID-19 screening or testing if referred by a Kaiser Permanente doctor. 
       * If you test positive with COVID-19, additional services, including hospital admission (if applicable), you will be responsible for costs according to your standard plan coverage details.
     * For additional information: [Kaiser Coronavirus Resource Center](https://healthy.kaiserpermanente.org/northern-california/health-wellness/coronavirus-information).
   * UHC:
     * For [telehealth](https://www.myuhc.com/member/jsp/preMain.jsp), please sign into your account.
     * Mail order prescription delivery is available through Optum Home Delivers. Sign up through the UHC online portal. 
       * Receive up to a 90-day supply of covered maintenance drugs (prescribed to treat chronic or long-term medical conditions, such as asthma, diabetes, high blood pressure).
     * [Treatment Costs](https://www.uhc.com/health-and-wellness/health-topics/covid-19).
       * $0 out-of-pocket costs (copay, coinsurance, deductibles) for COVID-19 testing and telehealth services, doctors visits for screenings, ER or hospital visits for screenings, and urgent care for screenings. 
         * UHC has waived costs for COVID-19 testing and use of telehealth services through June 18, 2020. 
         * Waiving of COVID-19 testing and telehealth costs *includes* the HSA-eligible plan--even before you meet your deductible (essentially, it’s being treated as preventive care), per [IRS Notice 2020-15](https://www.irs.gov/newsroom/irs-high-deductible-health-plans-can-cover-coronavirus-costs).
       * If you test positive for COVID-19, your care will be covered and your standard plan benefits will kick-in. You will be responsible for any out-of-pocket costs related to your treatment.
     * For additional information: [UHC Coronavirus Resource Center](https://www.uhc.com/health-and-wellness/health-topics/covid-19).
* FirstMark and Ro are offering [free COVID-19 telehealth assessments](https://covid.ro.co/firstmark/).
* Emergency Dental Care - Cigna
   * Many dental offices are limiting appointments for emergency needs only or have closed due to state and local mandates. In the event of a dental emergency (severe pain, acute infection, swelling, and/or persistent bleeding), here’s how to get help:
     * First, contact your dentist to determine their care options.
     * If the dental office is closed, Cigna can help you find care. Call 1-800-244-6224 or go to [mycigna.com](mycigna.com).
     * View additional details from [Cigna](http://images.connecting.cigna.com/Web/CIGNACorporation/%7B639647dd-3a01-4125-b82d-c7ab21d09604%7D_Receiving_Emergency_Dental_Care_-_FOR_PDF.PDF).
* All US Team Members are automatically enrolled in [UHC's Employee Assistance Program](/handbook/benefits/inc-benefits-us/#employee-assistance-program) by being enrolled in the Life/Disability insurance. This program offers help for difficult times including counseling services.

### United Kingdom

* [Patient Access](https://www.patientaccess.com/) has an online symptom checker to see if you need medical help if you think you may have COVID-19. You may also have the option to connect with your General Practitioner through a [video consultation](https://support.patientaccess.com/appointments/video-consultations).

### Canada

* Ontario: Recommends to take their [self-assessment](https://www.ontario.ca/page/2019-novel-coronavirus-covid-19-self-assessment) and if you answer yes to the questions, contact your primary care provider or Telehealth Ontario (1-866-797-0000) to speak with a registered Nurse.
* Quebec: Recommends to call 1-877-644-4545 if you have a cough or fever. [For more information](https://www.quebec.ca/en/health/health-issues/a-z/2019-coronavirus/#c46341).
* Manitoba: Recommends to call 204-788-8200 or 1-888-315-9257 if you have symptoms of COVID-19. [For more information](https://www.gov.mb.ca/covid19/).
* Saskatchewan: Recommends to take their [self-assessment](https://www.saskatchewan.ca/government/health-care-administration-and-provider-resources/treatment-procedures-and-guidelines/emerging-public-health-issues/2019-novel-coronavirus/covid-19-self-assessment) and contact HealthLine 811 if you have symptoms and have travelled outside Canada within the last 14 days. [For more information](https://www.saskatchewan.ca/coronavirus#utm_campaign=q2_2015&utm_medium=short&utm_source=%2Fcoronavirus).
* Alberta: Recommends to take their [self-assessment](https://myhealth.alberta.ca/Journey/COVID-19/Pages/COVID-Self-Assessment.aspx) and contact Health Link 811 if you have symptoms or have travelled outside Canada. [For more information](https://www.alberta.ca/coronavirus-info-for-albertans.aspx).
* British Columbia: Recommends to contact your healthcare provider or HealthLinkBC (811) if you have symptoms and have been in contact with someone who has tested positive.

### Netherlands

* For information on COVID-19, there is a national information line: 0800-1351.
* If you believe you have COVID-19, it is recommended to call your family doctor especially if you have an elevated temperature or have been to a high-risk area. [For more information](https://www.rivm.nl/en/novel-coronavirus-covid-19).

### Germany

* If you suspect you have COVID-19, it is recommended to contact your [local health office](https://tools.rki.de/PLZTool/de-DE), contact your primary doctor, or contact the non-urgent medical assistance line: 116117.

### Australia

* For information on COVID-19, there is a Coronavirus Helpline: 1800 020 080.
* If you believe you may have COVID-19, you can use the [online symptom checker](https://www.healthdirect.gov.au/symptom-checker/tool/basic-details). It is recommended that you call your doctor in advance before going to their clinic if you think you may have COVID-19.

### Aotearoa New Zealand

* For COVID-19 health advice and information, contact the Healthline team (for free) on 0800 358 5453 or +64 9 358 5453 for international SIMS.
* Please see the [Ministry of Health information page](https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus) for updated information.

### India

* If you have symptoms of COVID-19 and have travelled to an affected country, you can call the Ministry of Health & Family Welfare, Government of India's helpline: 011-2397 8046. If you qualify for testing, they will direct you to a [government-approved lab](https://www.icmr.nic.in/). [For more information](https://www.mohfw.gov.in/).
* Central Helpline Number for corona-virus: +91-11-23978046
* [Helpline Numbers of States & Union Territories (UTs)](https://www.mohfw.gov.in/coronvavirushelplinenumber.pdf)

### Ireland

* For general information, you can contact HSELive: 1850 24 1850. If you believe you have COVID-19, it is recommended to call your General Practitioner.

### South Africa

* The Department of International Relations and Cooperation (DIRCO) has set up a 24-hour hotline number for all South African citizens that believe they might have COVID-19, the hotline number is 0800-029-999.
