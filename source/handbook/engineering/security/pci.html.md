---
layout: handbook-page-toc
title: "GitLab PCI Compliance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is PCI?

PCI is the shorthand for the Payment Card Industry's Data Security Standard (PCI-DSS) as defined by the [PCI security standards council](https://www.pcisecuritystandards.org/). The PCI-DSS defines the requirements of businesses that accept or facilitate credit card payments based on type or amount of transactions accepted or facilitated.

## What are GitLab's requirements for PCI compliance?

GitLab is an open core solution that provides both a free and tiered paid plans that primarily utilizes credit card payments for subscriptions. Because of the low number of credit card transactions GitLab is involved with and the fact that we use a third-party payment processor, GitLab is currently a level 4 merchant for PCI. Level 4 merchants are required to complete an annual self-attestation questionnaire (SAQ) and since we fully outsource payment processing, specifically the SAQ-A. Quarterly scanning of our PCI systems must also be performed by an approved scanning vendor (ASV), GitLab uses Tenable.io.

## What is GitLab's current state of PCI compliance?

GitLab has completed an SAQ-A form and regularly performs scans of our in-scope PCI systems by an ASV. The SAQ-A form (and associated Attestation of Compliance (AoC)) and the results of our ASV scans are available upon request. Please send an email to [security@gitlab.com](mailto:security@gitlab.com) to request these documents.

## What is considered a PCI system?

Determining how a system may interact with the payment processing system is important when defining which systems are in-scope for PCI compliance.  The following diagram demonstrates in-scope (connected-to) vs. out-of-scope (not connected-to) when defining the PCI environment:

```mermaid
graph TB

  SubGraph2Flow
  subgraph "Out-of-Scope B"
  SubGraph2Flow(System D)
  end

  SubGraph1Flow
  subgraph "Out-of-Scope A"
  SubGraph1Flow(System C)
  end

  subgraph "In-Scope for PCI"
  Node1[Payment Processing System] --> Node2[Connected-To System A]
  Node1[Payment Processing System] --> Node3[Connected-To System B]
  Node2 --> SubGraph1Flow(System C)
  Node3 --> SubGraph2Flow(System D)
end
```
**Please Note:**

*  Connected-to systems create a buffer for the out-of-scope systems from directly connecting to the payment processing system.
*  It does not matter where a system resides (i.e. different cloud environment), if it communicates with the payment processing system, it is in-scope for PCI.

## GitLab's PCI Environment

```mermaid
graph TB
  Node10[External Facing Host]
  Node11[Internal Facing Host]


  SubGraph2Flow
  subgraph "Out-of-Scope B"
  SubGraph2Flow(System D)
  end

  SubGraph1Flow
  subgraph "Out-of-Scope A"
  SubGraph1Flow(System C)
  end

  subgraph "In-Scope for PCI"
  SubGraph3Flow[customers.gitlab.com] -- GitLab Internal OAuth --> Node2[dev.gitlab.org]
  SubGraph3Flow[customers.gitlab.com] -- GitLab Configuration Management --> Node3[Chef Server]
  SubGraph3Flow[customers.gitlab.com] -- Security Logging --> Node7[Gitlab-PCI]
  Node2 --> SubGraph1Flow(System C)
  Node3 --> SubGraph2Flow(System D)
  end
  

  subgraph "External PCI Vendors"
  Node4[Stripe] -- Zuora iFrame --> SubGraph3Flow[customers.gitlab.com]
  Node5[Zuora] -- Zuora iFrame --> SubGraph3Flow[customers.gitlab.com]
  sub[Shopify-swag.gitlab.com]
  end
  
  style SubGraph3Flow fill:#ffcccc
  style Node11 fill:#a2f2a9
  style Node10 fill:#ffcccc
	style Node7 fill:#a2f2a9 
  style Node5 fill:#ffcccc
  style Node4 fill:#ffcccc
  style Node3 fill:#ffcccc
  style Node2 fill:#ffcccc
  style sub fill:#ffcccc
  style SubGraph2Flow fill:#a2f2a9
  style SubGraph1Flow fill:#ffcccc
```

### Systems Detail 

| System               	| Purpose                                                                                                                 	| Location      	| Scope            	| Reasoning                                                                                                 	|
|----------------------	|-------------------------------------------------------------------------------------------------------------------------	|---------------	|------------------	|-----------------------------------------------------------------------------------------------------------	|
| customers.gitlab.com 	| GitLab system for providing customers the ability to create and pay for tiered product subscriptions with a credit card 	| Azure         	| PCI              	| Displays the Zuora iFrame to create/manage subscriptions and to process credit card payments using Stripe 	|
| dev.gitlab.org       	| Gitlab system that hosts the GitLab team member OAuth                                                                   	| Azure         	| Connected-To PCI 	| Provides GitLab team members secure access to customers.gitlab.com to maintain and support                	|
| Chef Server          	| GitLab system for configuration management                                                                              	| Digital Ocean 	| Connected-To PCI 	| Provides configuration management for customers.gitlab.com                                                	|
| gitlab-pci          	| GitLab system for external logging of customers.gitlab.com (fully segmented GCP project)                                	| GCP           	| Connected-To PCI 	| Logs of customers.gitlab.com must be shipped off the system in a secured environment                      	|
| Stripe               	| Third-party payment processor                                                                                           	| Stripe        	| PCI              	| Payment processor                                                                                         	|
| Zuora                	| Third-party subscription management platform                                                                            	| Zuora         	| PCI              	| Subscription management platform                                                                          	|
| Shopify              	| Third-party payment processor and swag.gitlab.com host                                                                  	| AWS           	| PCI              	| Hosts swag.gitlab.com                                                                                     	|

## Service Provider Matrix

This is a list of service providers who handle credit card transactions on behalf of GitLab:

|  Company/Product Name | Service Provided | Risk Assessment | Data Shared | Data Encryption Used | Written Agreement | PCI Validation | Expiration Date | Requirements Responsible For |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|  Shopify | E-Commerce Platform | Shopify Risk Acceptance | email address, first and last name, shipping address, phone number, and credit card information | in-transit: TLS 1.2+, at-rest: Merchant and customer data is encrypted at rest and sensitive information is further encrypted at the application layer. | [Shopify ToS](https://www.shopify.com/legal/terms) | Shopify AOC | 2019-7-21 | 6 and 7 |
|  Stripe | Payment Processor | Stripe DPIA |  | in-transit: TLS 1.2; at-rest: AES-256 | [Stripe SSA](https://stripe.com/ssa) | Stripe AOC | 2020-2-29 | 6, 7, 11 and 12 |
|  Zuora | E-Subscription Platform | Zuora DPIA |  | in-transit: AES 256 TLS; at-rest: server-side encryption (SSE) with AWS Key Management Service (SSE-KMS) | Zuora MSA (on file) | Zuora AOC | 2019-6-27 | 5, 6, 7, 10, 11 and 12 |


#### Matrix Key

The following table correlates each column of data back to its specific control:

<table class="tg">
  <tr>
    <th class="tg-uys7">Column</th>
    <th class="tg-uys7">Control</th>
  </tr>
  <tr>
    <td class="tg-baqh">Company/Product Name</td>
    <td class="tg-s6z2" rowspan="2"><a href="https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.01_third_party_assurance_review.md">TPM.1.01 - Third Party Assurance Review</a></td>
  </tr>
  <tr>
    <td class="tg-baqh">Service Provided</td>
  </tr>
  <tr>
    <td class="tg-uys7">Risk Assessment</td>
    <td class="tg-uys7" rowspan="3"><br><a href="https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.02_vendor_risk_management.md">TPM.1.02 - Vendor Risk Management</a></td>
  </tr>
  <tr>
    <td class="tg-uys7">Data Shared</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Data Encryption Used</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Written Agreement</td>
    <td class="tg-c3ow"><a href="https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.2.02_vendor_non-disclosure_agreements.md">TPM.2.02 - Vendor Non-Disclosure Agreement</a></td>
  </tr>
  <tr>
    <td class="tg-c3ow">PCI Validation<br></td>
    <td class="tg-uys7" rowspan="3"><a href="https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.04_vendor_compliance_monitoring.md">TPM.1.04 - Vendor Compliance Monitoring</a></td>
  </tr>
  <tr>
    <td class="tg-baqh">Expiration Date</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Requirements Responsible For</td>
  </tr>
</table>

## ASV Scanning

PCI Requirement 11.2 requires periodic unauthenticated scanning of a merchants externally-facing in-scope PCI environment to identify vulnerabilities that can potentially be exploited. The PCI-DSS council certifies Approved Scanning Vendors (`ASV`) to provide the ability to scan a merchants external PCI environment and provide an independent review of potential risk. ASV scanning is conducted on a quarterly basis and must demonstrate results that pass a third-party assessment which is completed by the ASV. For GitLab that is Tenable.io.

[PCI ASV Program Guide](https://www.pcisecuritystandards.org/documents/ASV_Program_Guide_v3.0.pdf)

### Vulnerability Reporting

#### Report Submission

Each quarter the merchant must submit to the ASV scan results to validate the external PCI environment is in compliance by the report not containing "High" or "Medium" severity vulnerabilities or any features or configurations that conflict with PCI-DSS.  Vulnerability severity levels are based on the National Vulnerability Database (`NVD`) and Common Vulnerability Scoring System (`CVSS`).

Report submissions for ASV attestation will occur on the following dates each year(coinciding with the GitLab fiscal calendar):
* February 1st
* May 1st
* October 1st
* January 1st

{-Please Note-}: First submission will be made by 2020-06-01 to allow for remediation timeframe to be completed.

The four previous passing scans will be submitted to requesting PCI acquirer with the latest PCI SAQ-A in the month of February.

#### Meeting External Scan Compliance

Vulnerability reports must be able to demonstrate compliance on two levels:

* Overall compliance of the merchant's external PCI environment
* No vulnerability with an assigned CVSS score of 4.0 or higher

#### Report Format

The scan report must have the following three sections:
1. Attestation of Scan Compliance
   * This is the overall summary that shows whether the scan customer’s infrastructure met the scan requirements and received a passing scan. 
2. ASV Scan Report Summary
   * This section of the scan report lists vulnerabilities by component (IP address, hosts/virtual hosts, domains, FQDN, etc.) and shows whether each component scanned received a passing score and met the scan requirement.
3. ASV Scan Vulnerability Details
   * This section of the scan report is the overall listing of vulnerabilities that shows compliance status (pass/fail) and details for *all* vulnerabilities detected during the scan.

#### Scan Results

A completed scan has one of four results:

1. A passing scan 👍`This is the only result that will be accepted to comply with quarterly external ASV requirement`
2. A failing scan for which the scan customer disputes the results ⚠️
3. A failing scan that the scan customer does not dispute 👎
4. A failing scan due to scan interference ❓

#### False Positives

The scan customer may dispute the findings in the ASV scan report including, but not limited to:
* Vulnerabilities that are incorrectly reported (false positives)
* Vulnerabilities that have a disputed CVSS Base score
* Vulnerabilities for which a compensating control is in place
* Exceptions in the scan report
* Conclusions of the scan report
* List of components designated by scan customer as segmented from the CDE
* Inconclusive ASV scans or ASV scans that cannot be completed due to scan interference

A dispute must be submitted in writing and can include screen captures, configuration files, system versions, file versions, list of installed patches, etc.

#### Compensating Controls

Another method of results dispute is demonstrating the use and implementation of compensating controls of identified scan results by the ASV. 

#### ASV Scanning Workflow Diagram

```mermaid
graph TD
	A(Determine Scan Scope) --> B[Provide list of identified<br/>external facing IPs to ASV]
	B --> C[Attest that scan scope includes<br/>all in-scope systems for PCI]
	C --> D[Configure active prevention systems<br/>from interfering with ASV scan]
  D --> E[Perform discovery scan]
  E --> F{Find web servers<br/>without domains or components<br/>not provided by scan customer?}
  F --> |Yes| G{Attest that component<br/>out of scope due to<br/>network segmentation}
  F --> |No| I
  G --> |No| B
  G --> |Yes| H[Attest that network<br/>segmentation is in place]
  H --> I[Perform ASV Scan]
  I --> J[ASV attests that<br/>PCI and ASV QA processes<br/>were followed]
  J --> K{PCI DSS<br/>compliant scan?}
  K --> |Yes| L>Compliant Scan Report]
  L --> M(Submit report<br/>as directed by acquirer)
  K --> |No| N{Dispute Scan Result?}
  N --> O>Non-Compliant Scan Report]
  O --> P[Fix issues and rescan<br/>until passing scan received]
  N --> |Yes| Q[Provide supporting evidence<br/>for dispute findings]
  Q --> R[Attest that all evidence submitted<br/>is accurate and complete]
  R --> S{Can ASV remotely validate evidence?}
  S --> |No| T{Is evidence sufficent?}
  T --> |No| P
  T --> |Yes| U[Any exceptions, special notes, etc.<br/>are included in the report]
  S --> |Yes| U
  U --> M

style A fill:#99ccff
style G fill:#ffff99
style F fill:#ffff99
style K fill:#ffff99
style N fill:#ffff99
style S fill:#ffff99
style T fill:#ffff99
style L fill:#a2f2a9
style O fill:#ffcccc
style U fill:#a2f2a9
style P fill:#ffcccc
style M fill:#99ff66
```

## Who can I reach out to if I have additional questions about PCI as it relates to GitLab?

The [GitLab security compliance team](/handbook/engineering/security/compliance.html#contact-the-compliance-team) can help with any questions relating to PCI.
