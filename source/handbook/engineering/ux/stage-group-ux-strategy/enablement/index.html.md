---
layout: handbook-page-toc
title: "Enablement UX Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
The Product Designers for Enablement focus on Distribution, Ecosystem and Geo. While these are quite different areas with their own Jobs to Be Done, the over-arching theme is making GitLab easier to install, use and and integrate, so that people can get started fast and without friction, and use GitLab where and when it works for them. GitLab believes that **everyone can contribute**, and this is central to our strategy.

[Enablement Product Direction](https://about.gitlab.com/direction/enablement/) 
[Enablement Engineering Pages](https://about.gitlab.com/handbook/engineering/development/enablement/)

### UX team members

* [Jacki Bauer](/company/team/#jackib) - UX Manager
* [Sunjung Park](/company/team/#sunjungp) - Product Designer, Geo and Distribution
* [Libor Vanc](/company/team/#lvanc) - Senior Product Designer, Ecosystem
* [Evan Read](/company/team/#eread) - Senior Technical Writer
* [Eileen Ruberto](/company/team/#eileenux) - Senior UX Researcher

### How we work
We follow the workflows outlined in the UX section of the handbook. In addition,
* we have an [issue board](https://gitlab.com/groups/gitlab-org/-/boards/1254585?label_name[]=UX&label_name[]=devops%3A%3Aenablement) that shows what we are up to. 
* we lable our issues with the UX, devops::enablement, and group:: labels.


## UX Scorecards
All UX Scorecards for the Enablement teams can be found in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/2166).

## Our strategy
The Enablement UX team will work with PMs to uncover customer's core needs and workflows. Becoming strategic involves gathering research, looking for patterns, and making plans for the best path forward for our customers and users. It is also about deciding what we value most, and how to best work together to achieve our goals.

