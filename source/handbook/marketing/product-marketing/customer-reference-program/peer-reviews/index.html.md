---
layout: markdown_page
title: "Peer Reviews"
---
## Peer Reviews

GitLab actively manages its Peer Review presence to maintain its values of Transparency, Collaboration, and Iteration.  By maintaining Peer Reviews, GitLab has a clear grasp of customer expectations.  Consistent maintenance of Peer Review presence maintains an updated understanding of the Voice of the Customer and helps GitLab’s Customer Success, Development, Marketing, and Sales teams to take maximum advantage of these valuable assets offered by their most valuable asset: the customers.

### Current Available Quote Assets

Current GitLab team members can view active Peer Review Quote Assets [here](https://docs.google.com/presentation/d/13tHJ1BgABMTwt0gZeP_hZK0B96Ui2FUrMWFVmFtT9Nw/edit?usp=sharing), the process to use these assets is outlined in the link, but also below:

* notify jparker@gitlab.com or @jparker in slack when using a quote, as assets expire and need to be tracked
* the quotes most be used exactly as provided, with the URL intact
* if you have a specific request, see the next section

### Requesting Quote Asset Research in an Issue

* To submit a specific request, submit an issue request on the [Customer Reference Program Issue Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?label_name=Customer%20Reference%20Program) 
* Make sure you use the labels "Customer Reference Program" and "Online Customer Review" along with other necessary labels, and assign it to @jlparker 
* please give at least a 5-7 days notice, to give proper lead time
* if your request is urgent, please @jparker in Slack  


### Roadmap for Peer Reviews

*Goal: to set the standard in using Peer Reviews as a full customer listening tool*

|  **Stage 1: Current**  | **Stage 2: Evolve**  | **Stage 3: Firehose** | 
|---|---|---|
| Bring in new reviews to CMS under Product Marketing project, listed Epics under Marketing; Public Response for each curated review *(Values: Collaboration, Transparency)*; Get Feedback from broader PMM team re:labels and systematic CMS approach, Ruby scripting; PathFactory insights; Develop Quote Assets by Request | Move to New Project; Rework labels for Brevity and Color coding; Add aditional Epics for additional Peer Review Sites; Develop Relationships with additional Peer Review Sites; Create Vision Page, Ruby Scripting, and updated Handbook page based on Feedback | Use key contacts in other departments to funnel functionally derived information from Peer reviews (Customer Success, Product Development); Send information that achieves critical mass through appropraite channels (meetings, issues, presentations) to improve GitLab's service *(Values: Iteration, Collaboration)* |  
