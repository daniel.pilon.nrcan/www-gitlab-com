---
layout: markdown_page
title: "Usecase: Continuous Integration"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Continuous Integration

The Continuous Integration (CI) usecase is a staple of modern software development in the digital age. It's unlikely that you hear the word "DevOps" without a reference to "Continuous Integration and Continuous Delivery" (CI/CD) soon after. In the most basic sense, the CI part of the equation enables development teams to automate building and testing their code.

When practicing CI, teams collaborate on projects by using a shared repository to store, modify and track frequent changes to their codebase. Developers check in, or integrate, their code into the repository multiple times a day and rely on automated tests to run in the background. These automated tests verify the changes by checking for potential bugs and security vulnerabilities, as well as performance and code quality degradations. Running tests as early in the software development lifecycle as possible is advantageous in order to detect problems before they intensify.

CI makes software development easier, faster, and less risky for developers. By automating builds and tests, developers can make smaller changes and commit them with confidence. They get earlier feedback on their code in order to iterate and improve it quickly increasing the overall pace of innovation. Studies done by DevOps Research and Assessment (DORA) have shown that [robust DevOps practices lead to improved business outcomes](https://cloud.google.com/devops/state-of-devops/). Out of the "DORA 4" metrics, 3 of them can be improved by using CI:
- **Deployment frequency:** Automated build and test is a pre-requisite to automated deploy.
- **Time to restore service:** Automated pipelines enable fixes to be deployed to production faster reducing Mean Time to Resolution (MTTR)
- **Change failure rate:** Early automated testing greatly reduced the number of defects that make their way out to production.

[GitLab CI](/product/continuous-integration/) comes built-in to GitLab's complete DevOps platform delivered as a single application. There's no need to cobble together multiple tools and users get a seamless experience out-of-the-box.

<!--
facts about approaching 100mil software developers...
-->

## Personas

### User Persona

The typical **user personas** for this usecase are the Developer, Development team lead, and DevOps engineer.

#### Software Developer [Sacha](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)

Software developers have expertise in all sorts of development tools and programming languages, making them an extremely valuable resource in the DevOps space. They take advantage of GitLab SCM and GitLab CI together to work fast and consistently deliver high quality code.

- Developers are problem solvers, critical thinkers, and love to learn. They work best on planned tasks and want to spend a majority of their time writing code that ends up being delivered to customers in the form of lovable features.

#### Development Team Lead [Delaney](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)

Development team leads care about their team's productivity and ability to deliver planned work on time. Leveraging GitLab CI helps maximize their team's productivity and minimize disruptions to planned tasks.

- Team Leads need to understand their team's capacity to assign upcoming tasks, as well as help resolve any blockers by assigning to right resources to assist.

#### DevOps Engineer [Devon](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)

DevOps Engineers have a deep understanding of their organization's SDLC and provide support for infrastructure, environments, and integrations. GitLab CI makes their life easier by providing a single place to run automated tests and verify code changes integrated back into the SCM by development teams.

- DevOps Engineers directly support the development teams and prefer to work proactively instead of reactively. They split their time between coding to implement features and bug fixes, and helping developers build, test, and release code.


### Buyer Personas

CI purchasing typically does not require executive involvement. It is usually acquired and installed via our freemium offering without procurement or IT's approval. This process is commonly known as shadow IT. When the upgrade is required, the [Application Development Manager](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) is the most frequent decision-maker. The influence of the [Application Development Director](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#dakota---the-application-development-director) is notable too.


### [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

## Industry Analyst Resources

Examples of comparative research for this use case are listed just below. Additional research relevant to this use case can be found in the [Analyst Reports - Use Cases](https://docs.google.com/spreadsheets/d/1vXpniM08Ql0v0yDd22pcNmXpDrA-NInJOwj25PRuHXA/edit?usp=sharing) spreadsheet.

- [Forrester Wave for Cloud-Native CI Tools](/analysts/forrester-cloudci19/)
- [Forrester Continuous Integration Tools](/analysts/forrester-ci/)

The AR Handbook page has been updated to reflect [how we plan to incorporate Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.




## Market Capabilities

| Market Capability | Description | Typical capability-enabling features | Value/ROI |
|---|---|---|---|
|1) Build and test automation| Streamline application development workflow by connecting simple, repeatable, automated tasks into a series of interdependent automatic builds and associated tests. Run and manage automated tasks in the background, preview and validate changes before it's merged to production. Ensure software is consistently built and tested without manual intervention, enabling developers to get rapid feedback if their code changes introduce defects or vulnerabilities. | CI/CD pipelines, scalable resources, job orchestration/work distribution, automated deployments, caching, external repository integrations, and the ability to run isolated, automated, tests (such as unit tests, regression tests, etc.). | Development teams can work with speed and efficiency. Catch potential errors sooner rather than later before they intensify. |
|2) Ease of configurations| The CI solution makes it easy for developers to automate the build and test workflow, specifically connecting to their source code repository, defining specific actions/tasks in build pipelines, and set parameters for exactly how/when to run jobs, using scripts and/or a GUI to configure pipeline changes. | Configurations via web UI or supports config-as-code in a human readable syntax, like YAML. | Maximize development time and improves productivity. Less manual work. |
|3) Easy to get started| The steps required to successfully setup CI should be simple and straightforward with minimal barrier to entry. The time and effort required for teams to get started from initial installation,  configuration, onboarding users, and delivering value should be short (days, not weeks). | Supports both on-premise and SaaS implementations. Has robust documentation outlining steps to connect to a repository and get a CI server up and running quickly. Supports configuration-as-code or provides a GUI for initial configurations. Web interface to provision users. | Faster time to value. |
|4) Supported platforms and languages| Many organizations use a mix of operating systems and code stacks within their development teams.  The CI solution must operate on multiple operating systems (Windows, Linux, and Mac).  The CI solution must also be language-agnostic supporting automation of build and test for a wide variety of development languages(Java, PHP, Ruby, C, etc.). | Option to use native packages, installers, downloads from source, and containers. Cloud-native platform support for public, private, or hybrid hosting. Runs on any OS. | Gives teams more flexibility, making it easier to adopt. |
|5) Visibility and collaboration| The solution enables development teams to have visibility into pipeline structure, specific job execution, and manage changes and updates to the pipeline jobs. The solution should enable teams to easily collaborate and make code changes, review work, and communicate directly. CI solutions should provide visibility and insights into the state of any build. | Pull requests or merge requests, commit history, automatic notifications/alerts, chatOps, code reviews, preview environments. | Faster feedback and less build breaking changes. |
|6) DevOps toolchain integrations| The solution supports strong integrations to both upstream and downstream processes such as project management, source code management and version control, artifact repositories, security scanning, compliance management, continuous delivery, etc. A CI solution with strong DevOps integrations means there's flexibility for users who need or want a balance between native capabilities and integrations. | Integrations with build automation tools (Gradle, Maven etc.), code analysis tools, binary repos, CD tools, IDEs, APIs, third party libraries or extensibility via plugins. | Increases efficiency. Lessens cost and the extra work that comes along with potential migrations. |
|7) Pipeline security| Access to the CI pipeline is managed and controlled in order to facilitate secure access, safely store/manage sensitive data (such as secrets), and ensure compliance in the event of an audit. | Access control (like RBAC), enterprise-based access systems (like LDAP), automated security scans and tests, data encryption, secrets management, and secure network connections. | Reduces business risk and protects intellectual property. Instills confidence in end-users. |
|8) Analytics| Capture and report on pipeline performance metrics and data throughout the build/test processes in order to identify bottlenecks, constraints, and other opportunities for improvement. | Build health, pipeline visualization, code coverage, logging, and performance metrics such as deployment frequency. | Improve efficiencies and reduce unnecessary costs. |
| tbd | tbd | tbd | tbd |

## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
| **1) Leading SCM and CI in one application** | GitLab enables streamlined code reviews and collaboration at proven enterprise scale, making development workflows easier to manage and minimizing context switching required between tools in complex DevOps toolchains. Users can release software faster and outpace the competition with the ability to quickly respond to changes in the market. | - Forrester names GitLab among the leaders in [Continuous Integration Tools in 2017](/analysts/forrester-ci/), Alteryx uses GitLab to have code reviews, source control, CI, and CD [all tied together](/customers/alteryx/), [Axway](/customers/axway/) overcomes legacy SCM and complex toolchain. |
| **2) Rapid innovation** | GitLab embraces an approach to rapid innovation that organizations undergoing digital transformations or adopting CI/CD also work diligently to implement internally with frequent, regular, releases delivered on a monthly basis. This provides end users with the latest and greatest in terms of capabilities/features, consistent security updates, and other incremental value adds over time. By GitLab "walking the talk" in regards to CI/CD, we understand the needs and pains that our users and organizations using GitLab face and share a mutual benefit from fully embracing this model centered around continuous improvement. | - GitLab deploys over 160 times a day and is one of the [30 Highest Velocity Open Source Projects](/blog/2017/07/06/gitlab-top-30-highest-velocity-open-source/) from the CNCF, we're voted as a [G2 Crowd Leader 2018](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader) with more than 170 public reviews and a 4.4 rating noting "Powerful team collaboration for managing software development projects," and have over 2,900 active contributors.  |
| **3) Built in security and compliance** | GitLab comes with security features out-of-the-box and automated security testing with audit controls to facilitate policy compliance. Moving security testing farther left into the SDLC catches potential problems earlier and shortens feedback loops for developers. This means a faster time to market delivering secure, compliant, code and an increase in customer confidence.  | - Gartner mentions GitLab as a vendor in the Application Monitoring and Protection profile in its [2019 Hype Cycle for Application Security](https://www.gartner.com/en/documents/3953770/hype-cycle-for-application-security-2019). [Wag! takes advantage of built-in security and faster releases with GitLab](/blog/2019/01/16/wag-labs-blog-post/), and [Glympse](/customers/glympse/) makes their audit process easier and remediates security issues faster. |



## Competitive Comparison
Amongst the many competitors in the DevOps space, [Jenkins](/devops-tools/jenkins-vs-gitlab.html) and [CircleCI](/devops-tools/circle-ci-vs-gitlab.html) are the closest competitors offering continuous integration capabilities.

## Proof Points - Customer Recognitions

### Quotes and reviews

#### Gartner Peer Insights

*Gartner Peer Insights reviews constitute the subjective opinions of individual end users based on their own experiences, and do not represent the views of Gartner or its affiliates. Obvious typos have been amended.*

>"We've migrated all our products from several "retired" VCS's to GitLab in only 6 months. - Including CI/CD process adoption - Without [loss] of code - Without frustrated developers - Without enormous costs"
>
> - **Application Development Manager**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1037269)
>
>"Finally, the most amazing thing about Gitlab is how well integrated the [GitLab] ecosystem is. It covers almost every step of development nicely, from the VCS, to CI, and deployment."
>
> - **Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1038051)
>
>"One of the best ways to approach source code management (Git) and release pipeline management [CI/CD]. [Gitlab] gives you a simple yet highly customizable approach to release management which is a complicated topic."
>
> - **System Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1045457)
>
>"The best software tool to manage all our projects and configure [CI/CD]. I will definitely recommend GitLab to everyone that wants to start a new project and doesn't want to use too many tools, GitLab has everything that you need to manage."
>
> - **Web Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1063698)
>
>"Over the years, we have successfully taken advantage of [Gitlab's] continuous deployment and integration mechanisms to build [and] maintain a robust and solid codebase."
>
> - **Co-Founder/CEO**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1111080)
>
>"One of the best [tools] for continuous integration and continuous deployment. "
>
> - **Lead - Mobile Apps**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1117401)
>
>"Overall, the experience with Gitlab is very positive, it provides many powerful features especially regarding Continuous Integration and [Continuous Deployment]"
>
> - **Developer Analyst**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1140016)
>
>"[GitLab's] UI is so easy and manageable to understand. Built-in continuous integration is one of its best [features]."
>
> - **Technical Evangelist**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1092444)
>
>"GitLab isn't just for hosting your code, it's for the entire lifecycle of your code. Since they host code, it makes sense for them to provide services around development and getting code into production. Their integration into other services is really easy. They give you GitLab-CI for any CI/CD needs, driven from a yaml file."
>
> - **Testing Analyst**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/886423)
>
>"This is really an amazing source code repository toolset which enables the robust CI practices which are inbuilt features of GitLab. This can be utilized well in any enterprise looking for the smooth CI/CD pipelines."
>
> - **Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/enterprise-agile-planning-tools/vendor/gitlab/product/gitlab/review/view/1009762)

### Blogs

**[Jaguar Land Rover](/blog/2018/07/23/chris-hill-devops-enterprise-summit-talk/)**
* **Problem:** JLR had **6 week feedback loops** resulting in slowdowns
* **Solution:** Premium (CI)
* **Result:** **Feedback loops reduced to 30 mins.** JLR is deploying within the engineering environment, 50-70 times per day of each individual piece of software to a target or to a vehicle.
* **Sales Segment:** Enterprise

**[Ticketmaster](/blog/2017/06/07/continous-integration-ticketmaster/)**
* **Problem:** Long Jenkins build times slowed down CI pipeline
* **Solution:** Premium (CI)
* **Result:** Less than 8 minutes total from commit to build, test and generate artifacts
* **Sales Segment:** Enterprise


### Case Studies

**[Goldman Sachs](/customers/goldman-sachs/)**  
* **Problem** Needed to increase developer efficiency and software quality 
* **Solution:** GitLab Premium (CI/CD, SCM) 
* **Result:** Improved from **1 build every two weeks to over a 1000/day**, or releasing 6 times per day per developer, and an average cycle time from branch to merge is now 30 minutes; simplified workflow and simplified administration
All the new strategic pieces of ’software development platforms are tied into GitLab. GitLab is used as a complete ecosystem for development, source code control and reviews, builds, testing, QA, and production deployments.
* **Sales Segment:** Enterprise

**[CERN](/customers/cern/)** 
* **Problem:** CERN was looking for an open source way to host their pipelines
* **Solution:** GitLab Starter (CI)
* **Result:** The European-based particle physics laboratory, uses GitLab for more than **12,000 users and 120,000 CI jobs a month**
* **Sales Segment:** Enterprise

**[Wish](https://about.gitlab.com/customers/wish/)**
* **Problem:** Wish was using TeamCity for CI and build management, but their automated jobs kept failing. The development team used a homegrown toolchain that included GitHub for SCM and Phabricator for code reviews. 
* **Solution:** GitLab Premium (CI)
* **Result:** **50% reduction in pipeline down time.** Wish has removed the bottlenecks associated with complex toolchains.
* **Sales Segment:** Mid-Market

### References to help you close
[SFDC report of referenceable Verify customers](https://gitlab.my.salesforce.com/a6l4M000000kDwk) Note: Sales team members should have access to this report. If you do not have access, reach out to the [customer reference team](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact) for assistance.

Request reference calls by pressing the "Find Reference Accounts" button at the top of your stage 3 or later opportunity.

## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this Use Case

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Resources

### Presentations
* [Why CI/CD?](https://docs.google.com/presentation/d/1OGgk2Tcxbpl7DJaIOzCX4Vqg3dlwfELC3u2jEeCBbDk)

### Continuous Integration Videos
* [CI/CD with GitLab](https://youtu.be/1iXFbchozdY)
* [GitLab for complex CI/CD: Robust, visible pipelines](https://youtu.be/qy8A7Vp_7_8)

### Integrations Demo Videos
* [Migrating from Jenkins to GitLab](https://youtu.be/RlEVGOpYF5Y)
* [Using GitLab CI/CD pipelines with GitHub repositories](https://youtu.be/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [Live Demo: GitLab CI Deep Dive](https://youtu.be/pBe4t1CD8Fc)
