---
layout: markdown_page
title: "Usecase: Simplify DevOps"
---

<!--

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
-->

## Simplify DevOps

The Simplify DevOps usecase is applicable for customers who are aware of the efficiencies to be gained by end to end DevOps but have not been able to achieve expected results due to a hodge podge of tools and integrations, siloed teams, cultural misalignment, lack of visibility and/or collaboration. In such scenarios, the **GitLab value of a single application** from idea to production inclusive of monitoring and security is appealing to the customer.  

The [worldwide DevOps market](https://www.researchandmarkets.com/reports/4856237/devops-market-global-industry-trends-share) is expected to grow with a **growth rate of 20 percent** from $3.5 Bn in 2018 to **$10.5 Bn in 2024**. Increasing adoption of agile frameworks, cloud technologies, and digitization of enterprises to automate business processes are driving the growth.


## Personas

### User Persona
Since there is a requirement for end of end DevOps improvements, the typical **user persona** for this usecase is [Devon - the DevOps Engineer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer). The key motivations for such a persona are:
- DevOps Engineers manage multiple different teams with varied needs. The motivation for Simplifying DevOps is to have a consistent and efficient development experience that is scalable with single source of truth and a shared view for collaboration to be able to support multiple teams.
- DevOps Engineers are frequently pulled in during crisis situations. The motivation for Simplifying DevOps is to test early, enforce common controls like single sign on and security processes as well as approvals to minimize post production issues.

### Buyer Personas
End to end DevOps requirements typically involve executive involvement - VP of DevOps, VP of Engineering, VP of Innovation and above.

## [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

## Analyst Coverage

- [Gartner - DevOps Hype Cycle 2019](https://www.gartner.com/en/documents/3947533/hype-cycle-for-devops-2019)
- [Forrester - Predictions 2020: DevOps](https://www.forrester.com/report/Predictions+2020+DevOps/-/E-RES157594)
- [Forrester - The Rise, Fall, And Rise Again Of The Integrated Developer Toolchain](https://go.forrester.com/blogs/the-rise-fall-and-rise-again-of-the-integrated-developer-tool-chain/)
- [Research & Markets - DevOps Market 2019-2024](https://www.researchandmarkets.com/reports/4856237/devops-market-global-industry-trends-share)

### [AR Plan](./ar-plan/)

  The AR Plan provides key details into how we intend to engage with the analyst community.

## UseCase Capabilities

tbd

## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
| **Single Application for Entire DevOps Lifecycle** | GitLab helps to eliminate toolchain complexity and deliver the complete DevSecOps capabilities through a single application using a single design system thereby minimizing context switching. | **-** [Gartner - 2019  Peer Insights Customers’ Choice - Enterprise Agile Planning Tools](/press/releases/2019-07-26-gitLab-recognized-in-gartner-peer-insights-customers-choice-for-EAPT.html) - Over 90 peer reviews with a 4.6 rating: [DevOps without the overhead](https://www.gartner.com/reviews/review/view/916200), [Simple, Intuitive and efficient DevOps Life Cycle tool](https://www.gartner.com/reviews/review/view/749177), and [GitLab increases engineering productivity](https://www.gartner.com/reviews/review/view/955945) |
| **Deploy your software anywhere** | GitLab is cloud agnostic (supporting GCP, AWS, Azure, OpenShift, VMware, On Prem, Bare Metal, etc)- helping support your choice of environment. GitLab offers a consistent workflow experience - irrespective of the environment. | **-** [Gartner 2019 Hype Cycle for DevOps](https://www.gartner.com/en/documents/3947533/hype-cycle-for-devops-2019) GitLab helped to define the market and is included as a relevant vendor for Toolchain, Toolchain Orchestration and Application Release Orchestration <br>**-** [Gartner 2019 Hype Cycle for Infrastructure and Operations Automation](https://www.gartner.com/en/documents/3947548/hype-cycle-for-i-o-automation-2019) GitLab helped to define the market and is recognized as a relevant vendor for both Continuous Delivery and Toolchain Orchestration  |
| **End to End Insight & Visibility** | GitLab using a common data model to provide configurable insights dashboards and cycle analytics to allow you to identify and drive areas of improvements of agile processes, cycle time, security and programs to help keep projects on track. | **-** [Forrester 2018 New Wave Value Stream Management - Strong Performer](/analysts/forrester-vsm/) - Value stream management provides visibility into project planning, health indicators and analytics to remove waste and focus on customer value. <br>**-** [G2 Crowd - 2018 - G2 Crowd Leader](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018) Over 170 public reviews with a 4.4 rating. [Powerful team collaboration tool for managing software development projects](https://www.g2.com/products/gitlab/reviews/gitlab-review-1976773), [GitLab is a great application for full Development Cycle. GitLab also provides a great dashboard](https://www.g2.com/products/gitlab/reviews/gitlab-review-1782675) |


## Competitive Comparison
Amongst the many competitors in the DevOps space, [GitHub](/devops-tools/github-vs-gitlab.html) and [Azure DevOps](/devops-tools/azure-devops-vs-gitlab.html) are the closest competitors offering end to end DevOps capabilities.

## Proof Points - customers

### Quotes and reviews
<List of customer quotes/reviews from public sites>

### Case Studies
* [BI Worldwide](/customers/bi_worldwide/) removed technology barriers to focus on building microservices.
> One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role.
>
> Adam Dehnel, Product Architect, BI Worldwide

* [Glympse](/customers/glympse/) consolidated ~20 tools consolidated into GitLab and remediated security issues faster than any other company in their Security Auditor's experience
>Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates and their pre-existing Docker-based deployment scripts.
>
>Zaq Wiedmann, Lead Software Engineer, Glympse

* [Goldman Sachs](/customers/goldman-sachs/) improves from bi-monthly builds to over a thousand per day
> GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day!
>
> Andrew Knight, Managing Director, Goldman Sachs

### References to help you close
<Link to SFDC list of use case specific references>

## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this Use Case

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Resources
### Presentations
* [Simplify DevOps - Single Application Message Customer Presentation](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit#slide=id.g639141d4c5_0_15)

### Simplifying DevOps Videos
* [Benefits of a single application](https://www.youtube.com/watch?v=MNxkyLrA5Aw)
* [GitLab in 3 minutes](https://www.youtube.com/watch?v=Jve98tlZ394)
* [Auto DevOps Click Through Demo Video](https://youtu.be/V_6bR0Kjju8?t=315)

### Integrations Demo Videos
* [Jira & Jenkins Integration Video](https://www.youtube.com/embed/Jn-_fyra7xQ)
* [How to setup the Jira Integration](https://www.youtube.com/watch?v=p56zrZtrhQE)
* [GitHub Integration Video](https://www.youtube.com/embed/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [All Marketing Click Through Demos](/handbook/marketing/product-marketing/demo/#click-throughs)
* [All Marketing Live Demos](/handbook/marketing/product-marketing/demo/#live-instructions)
