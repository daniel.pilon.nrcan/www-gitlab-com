---
layout: handbook-page-toc
title: "Field Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Field Marketing

The role of field marketing is to support marketing messages at a regional level through in-person interactions (quality vs. quantity) coupled with multi-touch activities. Field Marketing programs are focused on relationship building with customers and prospects to support land and expand opportunities as well as pulling pipeline through the funnel more quickly.

## Account Based Marketing
Account based marketing is seperate but sits next to field marketing. For info on account based marketing, please head over to the [ABM page](/handbook/marketing/revenue-marketing/field-marketing/account-based-marketing/).

# Types of programs Field Marketing runs

## GitLab Owned Field Events

### [GitLab Connect](https://www.youtube.com/watch?v=aKwpNKoI4uU)
GitLab Connect is a full or half day event with both customers and prospects in attendance sharing stories & lessons learned about GitLab. SAL's will be responsible for asking customers to speak and Marketing, through a combination of SDR outreach, database and ad geotargeting will drive attendance to the event. If you would like to propose a GitLab Connect in your city, please open an issue in the [Marketing - Field Marketing project](https://gitlab.com/gitlab-com/marketing/field-marketing) using the [`AMER_Event_Request_Template`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=AMER_Event_Request_Template) for AMER events or the [`Field Event_GitLabConnect`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=Field%20Event_GitLabConnect) template for all other regions. Interested in seeing a GitLab Connect in action? [Check it out.](https://www.youtube.com/watch?v=aKwpNKoI4uU)

To see how we handle GitLab owned events from a Marketing Programs standpoint, please head over to [their handbook page](/handbook/marketing/marketing-sales-development/marketing-programs/#event-channel-types)

### GitLab run workshops
Field Marketers will work with Product Marketing & Technical Product Marketing to put together various types of workshops depending on the needs of the region. Workshops available to date can be found [here](https://gitlab.com/gitlab-workshops). 

### 3rd party events
We will sponsor regional 3rd party events in an effort to build the GitLab brand and also to gather leads. Different type of 3rd part events include, but are not limited to:

- DevOps Days
- Agile Events
- City run technology meetings
- Customer/prospect run DevOps events on invite 
- Executive relationship building events via companies like Apex Assembly & Argyle Executive Forum

To see how we handle GitLab owned events from a Marketing Programs standpoint, please head over to [their handbook page](/handbook/marketing/marketing-sales-development/marketing-programs/#event-channel-types) 

### Field Event Goals

- Sales Acceleration
   - Engaging with existing customers
   - New growth opportunities
- Demand
   - Education
- Market Intelligence
   - Test out new messaging or positioning
 
### Other Tactics

Being the marketing expert for the region, GitLab Field Marketers are also responsible for using other tactics to help generate leads to build pipeline. We have options such as survey tools, both of current employees and past employees based on the 3rd party vendor we use, direct mailings, and also various digital tactics.

#### Digital tactics 

* To run plays where we are targeting a specific geography or where we would like to propose content syndication, we work through our Digital Marketing Programs team. Please create an issue [in their project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/tree/master/.gitlab%2Fissue_templates) if you'd like them to do work for you. 

* To run plays where we are targeting a specific list of accounts, we work with our ABM team utilizing [this issue template](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/blob/master/.gitlab/issue_templates/Demandbase_Campaign_Request_Template.md). 

#### Account Centric Integrated Micro-Campaign 

The purpose of the regional integrated micro-campaign is build and drive a specific strategy in a region (with a specific list of targted accounts/look-a-like accounts) based on intent data pulled from our ABM tool, DemandBase. 

##### Campaign Team 
* Field Marketing Manager - overarching DRI responsible for opening [the issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/Micro_Campaign.md), reviewing the intent data from the ABM DRI, and building the strategy based on what the intent data is suggesting (i.e. are a lot of folks searching to Kubernetes? Is it digital transformation? What call to action do we have? Do you want a direct mail piece involved? Do you want to drive to an On-Deamnd or scheduled webcast? etc), owning, and driving the mico-campaign. 

* Account Based Marketing (ABM) Manager - will work with FMM to produce target account list based on intent data and known targets & will also lead strategy session with Demandbase Campaign Manager, should the FMM decided its part of the strategy to deploy ads via Demandbase. 

* Sales Development Rep (SDR) - Will assist in building out the actual target people & build Outreach cadence to support agreed upon CTA. 

* Digital Marketing Programs - Can assist in deploying paid digital strategies (ex: display ads, paid search, paid social, and/or paid publisher placement) and geo focused ads via our digital agency. They can also help get our digital agency involved should the FMM want to engage with the agency. 


### Vendors who we work with in AMER 
We sometimes work with third party vendors for outreach, event production etc. Below is a list of whom we work with currently and the epic that tracks whom we have evaluated/worked with in the past.
* Emissary.io - in an effort to help sales gain account intelligence 
* Banzai - to supplement event recruiting 
* [FM vendor evaluation](https://gitlab.com/groups/gitlab-com/marketing/-/epics/441)

### AMER Field Marketing Event Venue Tracking
The below epic is for tracking venues we would like to utilize for future events, or as a way to evaluate event venues we have already worked with to note the pros and cons of various event space across different regions.
*  [AMER Field Marketing Event Venues Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/625)

### GitLab Company Information (Including Tax ID)
[Useful Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) 

### Corporate Memberships owned by GitLab Field Marketing 

* [AFCEA](https://www.afcea.org/site/) - Membership is handled by the Public Sector Field Marketing Manager. Account information is stored in the marketing 1Pass vault. 
* [ACT-ICT](https://www.actiac.org) - Membership is handled by the Public Sector Field Marketing Manager. Please ping the PubSec FMM for details if you'd like to join. 
 
## What's currently scheduled in my region?

| Region | FM DRI | GitLab User ID |
| :----- | :----- | :------------- |
| [AMER East NE & SE](https://gitlab.com/groups/gitlab-com/marketing/-/boards/915674?&label_name[]=East) | Ginny Reib | `@GReib` |
| [AMER East-Central](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1105137?&label_name[]=East%20-%20Central) | Jake Sorensen | `@JSorensen` |
| [AMER West-PacNorWest](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) | Rich Hancock | `@rhancock` |
| [AMER West - NorCal/SoCal/Rockies](TBD) | Rachel Hill | `@rachel_hill` |
| [AMER Public Sector](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933456?&label_name[]=Public%20Sector) | Helen Ortel | `@Hortel` | 
| [APAC](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933488?&label_name[]=APAC) | Pete Huynh | `@Phuynh` |
| [EMEA Southern Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1426531?&label_name[]=Southern%20Europe) | Tina Morwani | `@tmorwani` | 
 [EMEA MEA](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1426540?&label_name[]=MEA) | Tina Morwani | `@tmorwani` |
| [EMEA Northern Europe & Russia](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438252?&label_name[]=Northern%20Europe&label_name[]=Russia) | Kristine Setschin | `@ksetschin` |
| [EMEA UK/I](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438265?&label_name[]=UK%2FI) | Kristine Setschin | `@ksetschin` |
| [EMEA Central Europe & CEE](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438243?&label_name[]=Central%20Europe&label_name[]=EMEA&label_name[]=Europe%20CEE) | Jeffrey Smits | `@jsmits` |

**NOTE** to see the full list of events, you need to be logged into your GitLab account. There are times we make issues private. 

To find out what upcoming events GitLab will be involved in please visit our [Events Page](/events/). If you have any questions or an event suggestion please review [how to suggest an event at GitLab](/handbook/marketing/events/#suggesting-an-event)

For details on how we handle events & how to suggest an event for your region[(corporate or field, please check out this page)](/handbook/marketing/events/).

## AMER Field Marketing iPad Purchasing and Setup Instructions
 
### Purchasing Details  
 
[iPad Pro 12.9 inch/256GB/wifi](https://www.apple.com/shop/buy-ipad/ipad-pro/12.9-inch-display-256gb-space-gray-wifi)  
[iPad Pro 12.9 inch Smart Keyboard Folio](https://www.apple.com/shop/product/MU8H2LL/A/smart-keyboard-folio-for-129-inch-ipad-pro-3rd-generation-us-english)    
* When purchasing, please utilize the GitLab Business Account for corporate discounts. The Apple store/online representative will look up the GitLab Business Account associated with GitLab's 268 Bush St., San Francisco, CA  94104 address.
* Do not purchase AppleCare
* iPads can only be purchased by the regional Field Marketing Manager

### iPad Tracking 
To ensure we know who within the company currently has a company owned ipad, [please see here](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/1031). 
### Setting Up Logins
 
**iPad Password**  
* Utilize the password listed in the notes section of the AMER Field Marketing Apple ID located in the Marketing 1Pass
 
**Field Marketing Apple ID**  
* Log in to the AMER Field Marketing Apple account utilizing the username and password listed under the AMER Field Marketing Apple ID located in the Marketing 1Pass. Please take note of the additional information listed in the notes section regarding cell phone verification.

**Marketo Check-in App**  
* Download the Marketo app from the App Store. Sign in using your Marketo login and follow the instructions [HERE](/handbook/marketing/events/#marketo-check-in-app) for details on using the app during events.

**Google Drive and Slides**  
* Follow the instructions [HERE](/handbook/marketing/product-marketing/demo/conference-booth-setup/#ipads) to set up Google Drive and Slides

## Field Marketing Labels in GitLab
The Field Marketing team works from issues and issue boards. If you need our assistance with any project, please open an issue and use the `Field Marketing` label anywhere within the GitLab repo.

**General Field Marketing Labels:**  
`Field Marketing`: Issue initially created, used in templates, the starting point for any issue that involves Field Marketing  
`FY21-Q1`, `FY21-Q2`, `FY21-Q3`, `FY21-Q4`: What event or activity is set to take place or be due in this quarter  

**Regional Field Marketing Labels:**

* `APAC`: Issues that are related to the APAC sales team  
* `EMEA`: Issues that are related to the EMEA sales team  
* `Central Europe`: Issues that are related to the EMEA Central Europe (DACH) sales team  
* `Europe CEE`: Issues that are related to the EMEA Central Eastern Europe sales team  
* `Southern Europe`: Issues that are related to the EMEA Southern Europe (France, Italy, Spain, Portugal, Greece, Cyprus) sales team  
* `Northern Europe`: Issues that are related to the EMEA Northern Europe (Nordics & Benelux) sales team  
* `UK/I`: Issues that are related to the EMEA UK & Ireland sales team  
* `Russia`: Issues that are related to the EMEA Russia & Ukraine sales team  
* `MEA`: Issues that are related to the EMEA MEA (Middle East & Africa) sales team   
* `East`: Issues that are related to the East sales team   
* `East - Central`: Issues that are related to the US East-Central sales team  
* `East - NE`: Issues that are related to the US East- NE sales team   
* `East - SE`: Issues that are related to the East-SE sales team    
* `WEST`: Issues that are related to the US West sales team  
* `WEST - Bay Area`: Related to the US WEST Bay Area sales team  
* `WEST - BigSky`: Issues that are related to the US WEST Midwest sales team  
* `WEST - FM Planning`: Issues related to West FM planning  
* `WEST - FMM`: Issues related to the West FMM  
* `WEST - PacNorWest`: Issues that are related to the US WEST Pacific North West sales team  
* `WEST - SW`: Issues that are related to the US WEST Southwest and SoCal sales team  
* `Public Sector US`: Any issues related to US Public Sector  
* `FMM AMER PubSec`: All issues and tasks related to the FMM AMER for PubSec  
* `FMM-Other Tactics`: Issues related to non-event tactics
* `FMM-MicroCampaigns`: Issues related to regional integrated micro-campaign, which are built to drive a specific strategy in a region
* `AMER - DOD`: AMER DOD tactics  
* `AMER - CIV`: AMER Civilian tactics  
* `AMER - SLED`: AMER SLED tactics   
* `AMER - IC`: AMER National Security tactics  
* `FMC AMER NE/SE/PubSec`: Issues related to the FMC AMER East & PubSec      
* `FMC AMER NE/SE/PubSec - Tracking`: Deadline tracking related to FMC AMER East & PubSec   
* `FMC AMER NE/SE/PubSec - Swag`: FMC AMER East & PubSec tracking for swag and event assets   
* `FMC AMER - West/East-Central::Watching`: Issues that the FMC AMER West is tracking   
* `FMC AMER - West/East-Central::Working`: Issues that the FMC AMER West is actively working on   
* `FMC AMER - West/East-Central::Complete`: Issues that FMC AMER West has finished their needed work on   
* `FMC EMEA - Event Tracking`: Event/deadline tracking related to FMC EMEA  
  

For more information on how Field Marketing utilizes GitLab for agile project management, please visit [Marketing Project Management Guidelines](/handbook/marketing/#-marketing-project-management-guidelines).  

## The [Field Marketing Budget](https://docs.google.com/spreadsheets/d/1QC6P0VRWwJheOlGB-9bX8JIF8_4UY3h1cGVT_gacv5M/edit#gid=184318451&range=A1)  
Field Marketing manages its budget in a separate doc, that our Finance Business Partners pull into the master marketing budget. Specific instructions on how we manage our budget: 
1.	Each FMM will have a qtrly budget of money to spend based on territory pipeline needs. 
2.	Finance issues will NOT be approved unless all column details are filled in. Field Marketing is the DRI for all columns. 
3.	Every qtr finance will do a pull of this data and push it into the company rolling 4 qtr budget. Details on date of pull will be forthcoming. 
4.	The "Budget Export" tab will be used to export to the master marketing budget spreadsheet and should be used by each FMM to ensure they are staying within budget.
5.	Each expense should have its own row. 
6.	The entire cost of the program is to be budgeted for - Sponsorship + lead scanners + monitor + any other auxiliary cost that the company will incur.
7.	If the event is just 1 day, then the start and end date would be the same. 
8.	The Campaign tag auto populates based off of the program name and the end date (ISO format) of the campaign. The max of 37 characters that can be entered into NetSuite is already taken into consideration in the formula. 
9.	Each tab is protected based on the managers/DRI's for each region. Only those with permissions will be able to edit each tab. Please reach out to the marketing finance business partner if edit access needs to be changed.
10. Only the "FY 21 Spend" column (O) needs to be filled out with the total spend of the program. Equations are in the monthly columns (P-AA) that will evenly spread the total amount over the months that are in the Start and End date columns. However, the equation can be deleted and dollar values can be hard-coded into the cells if the spend is not expected to be evenly spread over the months of the event. 
11.	The MQL target is built off of a formula based on the type of activity the spend is. CXO focused = 70% of the leads will convert. Regional tradeshow = 30% of the leads will convert. ABM = 50% of the leads will convert. 
12.	There will be a swag line item per region per qtr, so each sub region will not need to take this into consideration when it comes to the cost of the activity. AMER: Reminder that the Nadel portal spits out swag costs upon order completion. 
13.	Created Net Suite tags can be found [here](https://docs.google.com/spreadsheets/d/19Le1PrWE1JbqN6Wz3D6tGfy7Ee5PmoetfyLPdwinFV0/edit?ts=5daf2dad#gid=248514497) 

## Field Marketing Communications
* If you're an employee at GitLab trying to reach Field Marketing, please head over to our slack channel [#fieldmarketing](https://gitlab.slack.com/archives/CCWDAJ8PK) and contact us using the `@field-marketing` slack alias with any questions you may have. 
     * **NOTE**: If you're requesting we sponsor an event, instructions can be found [here](/handbook/marketing/revenue-marketing/field-marketing/#whats-currently-scheduled-in-my-region).
* If a region specific question is asked in the [#fieldmarketing](https://gitlab.slack.com/archives/CCWDAJ8PK) slack room, please tag the [regional Field Marketing leader](#whats-currently-scheduled-in-my-region), as that person is the DRI. 

### Bi-Monthly calls 

* The World Wide Field Marketing team has two standing calls on the calendar. 
* The team meets on the 1st & 3rd Wednesday of each month. 
* The 1st call is EMEA time zone friendly and the 2nd call is APAC timezone friendly. 
* All team members are invited to each call although, if the call is scheduled outside of the team member's normal working hours, the team member is NOT expected to attend. The calendar invite simply serves as a reminder to the team member to check out the recording of the meeting the next working day and to also review the agenda. 
* The purpose of the team call is to share relevant company, marketing, and Field Marketing announcements.
* Anyone on the team should feel empowered to add content to the team meeting agenda.
* We will also have a specific `lessons learned` section where each Country Field Marketing Manager will select 1 FMM event or other tactic event recap to review.
    * The FMM who is the DRI for the event or other tactic we will be reviewing as a team will add the `Event Recap` link from the `Event Planning and Recap` to the agenda 
    * We review the recap prior to the call, the FMM DRI briefly gives a run down of the event or other tactic, then we dialogue.
    * Each region will be represented - AMER, APAC, and EMEA - so we will review 3 each call. 
* We also discuss use cases - could be how you've worked with social media, how you built a report in SFDC to help the team be more efficient, etc, 
* Guest Speakers - at times we will also invite other colleagues from the company to address our team as a whole
* As a handbook first company, if you are going to bring a topic to the team, please think twice on if you should add just an agenda item, or if you should add an agenda item that links to an MR or a handbook page. 
* If you're not on the Field Marketing team at GitLab and you're a GitLab employee who's interested in joining our team meeting, please feel free to ask in the #fieldmarketing slack room, we'd welcome the opportunity to host you! 

### Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we utilize a Slack plugin called [Geekbot](https://geekbot.io/) to coordinate status updates. Field Marketing world wide currently conducts 1 weekly standup. Geekbot shares this update in the public #fieldmarketing slack room.

#### Weekly Status Update
The **Weekly Status Update** is configured to run at 10 AM local time on Monday, and contains 5 questions:

1. ***What was your favorite part of the weekend?*** 

    The goal with this question is for you to get to know your colleagues and for you to be able to share what excited you from the previous few days when you weren't working.

2. ***What's happening in your life?*** 

    Is there anything you'd like the team to know about what's going in your life? Feel free to share as much or as little as you feel comfortable sharing. 

3. ***Are you traveling anywhere this week? If so, where and why.*** 

    As Field Marketers, we travel up to 50% of the time. Sharing where you are is important, especially if are in a different timezone. 

4. ***What are your top 1-3 priorities for the next week?***

    These top 3 priorities should be focused on what you plan to accomplish that week. 

5. ***Anything blocking your progress?*** 

    Of those 1-3 items listed, do you need any roadblocks removed in order to accomplish the priorities? 
    
You will be notified via the Geekbot plug in on slack at 10 AM your local time on Mondays, as stated above. It is important to note, that unless you answer all 5 questions in the Geekbot plug in, your answers will NOT be shared with your colleagues, so please be sure to complete all 5 questions! 

## Other pages to review for a full understanding of how Field Marketing at GitLab operates
* [Events at GitLab](/handbook/marketing/events/)
* [Marketing Program Management](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/)
* [Marketing Operations](/handbook/marketing/marketing-operations/)
* [Sales Development](/handbook/marketing/revenue-marketing/sdr/)
* [Links GitLab Field Marketers find useful](https://docs.google.com/spreadsheets/d/1gjJghF8Va-G0lYWsDaYXKG7JPtADLS2Jhrh8IVHkizQ/edit?ts=5d249a33#gid=1748424259&range=A1)
* [Field Marketing onboarding videos](https://drive.google.com/open?id=1m8ReMIiymMTqqk5PJAG7u_IG-Q5pkusV) - NOTE - these are also in the Field Marketing Onbaording issue that is kept in the [Marketing onboarding project](https://gitlab.com/gitlab-com/marketing/onboarding#onboarding)

