---
layout: handbook-page-toc
title: "Leadership Engagement Check-in"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Leadership Engagement - Check-in

**Introduction**
Engagement Check-Ins (also known as [Stay Interviews](https://www.forbes.com/sites/billconerly/2018/07/21/retain-more-employees-with-stay-interviews/#659422a2f18c)) are conducted to help leadership improve retention in their teams. It helps them understand why team members stay and what might cause them to leave. In an effective Engagement Check-In, people managers ask standard, structured questions in a casual and conversational manner. 

**Process**
*  **Cadence**: Schedule your Engagement Check-In at least every 6 months; 
* **Form**: Use sync communication via Zoom; 
* **Preparation**: Preparation should be done on both the manager and the team member’s part before the meeting. The manager should set expectations with their team members prior to the meeting to ensure that the team member feels as comfortable as possible heading into the conversation. Managers can include a copy of this Handbook page so their team member is fully aware of the specific questions that will be asked during the call. Ask them to review the questions and think about them prior to the call. If they want to type up a response, that is ok as well and create a starting place for the conversation. 
* **Questions**: Have a look at the below questions and take notes during the meeting. 
* **Follow up**: At the end of the conversation summarize and follow up on any actions. 


## Structure

### Opening [example]
Start the conversation by explaining the purpose of the meeting and why it’s important to you as a manager. 

“Thanks for taking the time to have this check-in conversation. Team member engagement is very important to me, and this talk is a way to better understand how things are going for you and how I can help to improve your experience at GitLab. I would like to talk with you about the reasons you stay with GitLab so I understand what I can do to make this a great place to work for you.”

### Questions
* It’s important to listen and gather feedback from the team member about how you and GitLab can retain them. 
* What do you like most about your job?
* What do you like least about your job?
* What makes for a great day at work for you?
* What keeps you here at GitLab?
* What would make you consider leaving GitLab?
* If you could change one thing about your job, team or company, what would it be?
* What can I do to support you?

### Scenarios (Situations and Reactions)

1. **Team member has expressed the desire to stay at GitLab and for +90% is engaged**
- I am so pleased to hear GitLab is a great place for you and really value your input going forward. 
- In case there’s a change in your engagement I want to make sure you can always speak up also outside of these engagement check-ins. 


2. **Team member expressed both reasons to stay but also reasons for leaving (50/50)**  
- I really want to thank you for your valuable input and feedback. 
- Summarizing what we have discussed I would love to work on an engagement plan so that we can make sure GitLab is a great place to work for you. 
- Let's follow up in one week and prepare items we can work on to improve your experience at GitLab.

3.**Team member expressed mostly reasons why they consider leaving and isn’t engaged**
- I really want to thank you for your valuable input and feedback. 
- Looking at your concerns I would love to work on an engagement plan so that we can make sure GitLab becomes a great place to work for you. 
- Let's follow up in one week and prepare items we can work on to improve your experience at GitLab.


4. **Other scenarios**
- If you have a scenario different from the above, and/or you need help with messaging, please work with your manager or [People Business Partner](https://about.gitlab.com/handbook/people-group/how-to-reach-us/).

### Closing [example]
To close the Engagement Check-In, summarize the key points discussed, and work with the team member to develop an engagement plan (if applicable). Be sure to end on a positive note.

“Let me summarize what I heard you say about the reasons you stay at GitLab, as well as reasons you might leave. Then, let’s develop a plan to make this a great place for you to work.
I appreciate you sharing your thoughts with me today. I am committed to doing what I can to make this a great place for you to work.”

If during the Engagement Check-In issues arise around health, performance, compensation etc. and you need assistance, please work directly with your manager or your [People Business Partner](https://about.gitlab.com/handbook/people-group/how-to-reach-us/).

### Follow Up
After the call has closed, capture some notes about what was discussed and share with the team member to ensure you heard their feedback correctly. It is important to do this for all team members - even if they had positive feedback about their engagement level. 

For team members who had positive feedback, you will want to continue to find opportunities that align to things that make for a positive experience for them. For team members that had constructive feedback, you will want to use your notes as a start of an engagement plan. 
