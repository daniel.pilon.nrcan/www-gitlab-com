// ==UserScript==
// @name         GitLab filters
// @namespace    https://gitlab.com/
// @version      0.1
// @description  Be able to filter checkboxes on GitLab
// @author       Viktor Nagy
// @match        https://gitlab.com/*
// @grant        none
// @require      https://code.jquery.com/jquery-3.4.1.slim.min.js
// ==/UserScript==

(function() {
    'use strict';
    let hasHidden = false

    function hideLines (filterText) {
        let negate = false
        if(filterText.slice(0,1)==='!') {
            negate = true
            filterText = filterText.slice(1)
        }
        console.log(filterText, negate)
        const regexFilter = new RegExp(filterText)
        $(".detail-page-description.content-block li.task-list-item").each((idx, item) => {
            const $item = $(item)
            if(!(negate ^ regexFilter.test(item.textContent.trim()))) {
                hasHidden = true
                $item.hide()
            }
        })
    }

    function unHide() {
        $(".detail-page-description.content-block li.task-list-item").show()
        hasHidden = false
    }

    function filterLines (ev) {
        ev.preventDefault();
        if(hasHidden) {
            unHide()
        }
        hideLines(filterInput.val())
        clearButton.removeClass('hidden')
        setTimeout(() => {
            submitButton.removeAttr('disabled').prop('disabled', false).removeClass('disabled')
        }, 100)
    }

    function clearFilters() {
        unHide()
        filterInput.val('')
        clearButton.addClass('hidden')
    }

    $('.nav.navbar-nav .search-form').parent().after(`<li class="header-new dropdown" data-track-event="click_dropdown" data-track-label="new_dropdown" data-track-value="">
<a class="header-new-dropdown-toggle has-tooltip qa-new-menu-toggle" id="js-onboarding-new-project-link" title="" ref="tooltip" aria-label="New..." data-toggle="dropdown" data-placement="bottom" data-container="body" data-display="static" href="/projects/new" data-original-title="New...">
<svg class="s16" viewBox="0 0 511 511.99982"  xmlns="http://www.w3.org/2000/svg"><path d="m492.476562 0h-471.976562c-11.046875 0-20 8.953125-20 20 0 55.695312 23.875 108.867188 65.503906 145.871094l87.589844 77.851562c15.1875 13.5 23.898438 32.898438 23.898438 53.222656v195.03125c0 15.9375 17.8125 25.492188 31.089843 16.636719l117.996094-78.660156c5.5625-3.710937 8.90625-9.953125 8.90625-16.640625v-116.367188c0-20.324218 8.710937-39.722656 23.898437-53.222656l87.585938-77.851562c41.628906-37.003906 65.503906-90.175782 65.503906-145.871094 0-11.046875-8.953125-20-19.996094-20zm-72.082031 135.972656-87.585937 77.855469c-23.71875 21.085937-37.324219 51.378906-37.324219 83.113281v105.667969l-77.996094 51.996094v-157.660157c0-31.738281-13.605469-62.03125-37.324219-83.117187l-87.585937-77.851563c-28.070313-24.957031-45.988281-59.152343-50.785156-95.980468h429.386719c-4.796876 36.828125-22.710938 71.023437-50.785157 95.976562zm0 0"/></svg>
</a><div id="filterContainer" class="dropdown-menu dropdown-menu-right">
<form id="filter-form">
<input type="text" id="filter-text" placeholder="hide lines with content">
<button id="vnagy-filter-submit" type="submit">Filter</button>
<button id="vnagy-filter-clear" class="hidden" type="reset">x</button>
</form>
</div>
</li>`);
    const submitButton = $('#vnagy-filter-submit')
    const filterInput = $('#filter-text')
    const clearButton = $('#vnagy-filter-clear')

    $('#filter-form').submit(filterLines)
    clearButton.click(clearFilters)
})();