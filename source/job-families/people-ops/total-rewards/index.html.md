---
layout: job_family_page
title: "Total Rewards"
---

The Total Rewards job family is responsible for all the tools available so that GitLab may be attract, motivate and retain team members. Total Rewards encompasses the elements of compensation, well-being, benefits, and recognition that, when done correctly, lead to optimal organizational performance. Total Rewards is designed strategically and executed in alignment with business goals, Total Rewards programs fuel motivated and productive team members that are appreciated and rewarded for their contributions, driving GitLab to ever greater success.

## Total Rewards Coordinator

### Job Grade
The Total Rewards Coordinator is a [grade 5](/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities
- HRIS data entry.
- Audit HRIS new hires to ensure accuracy.
- Process HRIS changes related to events, such as: hiring, termination, leaves, transfers, bonuses, or promotions.
  - Ensure all ancillary systems are up to date
  - Coordinate any changes with payroll
- Collaborate with the People Ops team on People Operations policies, processes, and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Update people operations documentation as directed.
- Keep it efficient and DRY.

## Total Rewards Analyst

### Job Grade
The Total Rewards Analyst is a [grade 6](/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities
- Coordinate on compensation strategies and principles.
- Participate in compensation and benefits surveys as directed.
- HRIS data entry.
- Audit HRIS new hires to ensure accuracy.
- Process HRIS changes related to events, such as: hiring, termination, leaves, transfers, bonuses, or promotions.
  - Ensure all ancillary systems are up to date
  - Coordinate any changes with payroll
- Collaborate with the People Ops team on People Operations policies, processes, and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Update people operations documentation as directed.
- Coordinate on benefits administration.
  - Audit and pay all US benefit invoices.
- Collect data to track trends in functional areas.
- Assist with training employees on various topics.
- Maintain approval requests in Greenhouse ensuring a job family and compensation benchmark are set.
- Keep it efficient and DRY.

### Requirements
- 2-5 years experience in an HR or People Operations role with a concentration on compensation and benefits
- Bachelor's degree in Mathematics, Business, or HR preferred
- Ability to work strange hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Enthusiasm for, and broad experience with, software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Desire to work for a fast moving startup
- You share our [values](/handbook/values), and work in accordance with those values
- The ability to work in a fast paced environment with strong attention to detail is essential.
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
- Ability to use GitLab

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
   * Next, candidates will be invited to a 45 minute interview with the Hiring Manager; Manager, Compensation & Benefits
   * Then, candidates will be invited to schedule a 45 minute interviews with the Manager, People Operations
   * Final, candidates will be invited to schedule a 50 minute interview with the Director, Global People Operations

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).

## Senior Total Rewards Analyst

### Job Grade
The Senior Total Rewards Analyst is a [grade 7](/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities
- Develop compensation strategies and principles.
- HRIS data entry, ensuring Data Integrity and alignment with all ancillary systems.
- Process HRIS changes related to events, such as hiring, termination, leaves, transfers, or promotions.
- Audit all system changes to ensure accuracy.
- Implement People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Maintain people operations documentation.
- Benefits management.
- Collect and analyze data to track trends in functional areas.
- Ensure compliance with all international rules and regulations.
- Manage special projects.
- Collaborate on training employees on various topics.
- Keep it efficient and DRY.

## Manager, Compensation & Benefits

### Job Grade
The Manager, Compensation & Benefits is a [grade 8](/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities
- Manage and implement global compensation strategies and principles.
- Global benefits management and development of benefit principles.
- Manage the design and development of tools to assist employees in benefits selection, and to guide managers through compensation decisions.
- Plan, direct, supervise, and coordinate work activities of direct reports relating to compensation, benefits, and people operations data.
- Mediate between benefits providers and employees, such as by assisting in handling employees' benefits-related questions or taking suggestions.
- HRIS management, ensuring Data Integrity and alignment with all ancillary systems.
- Audit all systems to ensure data accuracy.
- Develop and implement People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Manage people operations documentation.
- Iterate based on data trend findings.
- Ensure compliance with all international rules and regulations.
- Manage special projects.
- Keep it efficient and DRY.

## Director, Total Rewards

The Director, Total Rewards leads the compensation and benefits function ensuring our total rewards strategy and practices continue to support our culture and growth. This role is a senior member of the People Group team who is responsible for building out the Total Rewards function and mentoring current team members. The Director, Total Rewards reports to the Chief People Officer.

### Job Grade
The Director, Total Rewards is a [grade 10](/handbook/people-group/global-compensation/#gitlab-job-grades).

### Responsibilities
- Review and analyze GitLab’s total rewards (compensation, benefits, perks, recognition) practices globally and in relation to local markets; propose improvements to remain competitive
- Maintain expertise in industry practices and lead effective, competitive, and fair total rewards programs that ensure market consistency and cost-effectiveness
- Design, develop, and execute on a comprehensive global total rewards strategy that aligns with our culture and business plans, and allows GitLab to attract, retain, and motivate top talent
- Create scalable processes, practices, and programs that support our rapid growth while continuing to deliver an incredible employee experience
- Oversee and support the development, implementation, and maintenance or compensation tools and systems
- Oversee our benefits programs including insurance, retirement, time off, etc.
- Own the communications strategy and plan for total rewards, ensuring team members understand all that’s offered at GitLab
- Lead the team to perform effective job evaluations/leveling, conduct market surveys, collect and analyze market data, and maintain compensation survey data; continuously updating our compensation ranges and job structure
- Develop recommendations for annual budgeting and planning while maintaining internal and external equity in pay plans; manage annual compensation review cycles
- Develop and manage executive compensation; review, design, develop, and manage variable incentive pay for non-sales
- Lead stock administration and ensure accurate information is provided to team members about stock grants, vesting schedules, exercise process, and eligibility
- Manage equity planning and implementation of new schemes as needed; partner with Chief Financial Officer to manage the cap table
- Work closely with the Chief People Officer to develop material for executive management and the Compensation Committee
- Accurately interpret, counsel, communicate, and educate People Group members, managers, and executives on pay philosophy, policies, and practices

### Requirements
- Minimum 8 years of progressive experience in total rewards with in-depth knowledge of core compensation and job structure concepts and standard methodologies
- Experience designing and managing compensation and benefits programs, ideally at rapidly growing, global companies in a relevant industry
- Forward thinking, creative, and open-minded with sound technical skills, analytical ability, and seasoned judgment
- Comfortable and enthusiastic about working in a fast paced, high growth, constantly changing, geographically dispersed, transparent environment
- Ability to drive consensus and engagement across a wide variety of stakeholders in multiple parts of the business
- Data-driven leader with a strong ability to analyze and turn data into insights and action plans aligned with company direction
- Excellent verbal and written communication skills, ability to package and present complex analyses and recommendations clearly
- Previous experience in both public and startup companies; IPO experience is ideal
- Experience preparing for and interacting with the Compensation Committee
- Experience working remotely and with remote team members is preferred

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
   * Next, candidates will be invited to a 50 minute interview with the Hiring Manager
   * Then, candidates will be invited to schedule three separate 45 minute interviews with the Director, Global People Operations and the Manager, Compensation & Benefits and a People Business Partner
   * Final candidates will be invited to conduct two separate 50 minute interview with the Chief Financial Officer and another Executive

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).

## Performance Indicators
- [Percentage over compensation band](/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Pay equality](/company/culture/inclusion/#performance-indicators)
